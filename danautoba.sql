-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 05, 2017 at 03:07 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `danautoba`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(3) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id_category` int(3) NOT NULL,
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `category`) VALUES
(1, 'News'),
(2, 'Blog'),
(3, 'Story'),
(4, 'Tips & Trick');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id_contact` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `time_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id_contact`, `name`, `company`, `email`, `message`, `time_updated`) VALUES
(2, 'Hafidz', 'Idmetafora', 'info@idmetafora.com', 'Just Testing Message 2', '2017-08-18 01:52:54'),
(3, 'dsfa', 'kfdsal', 'hafidz.tkj@gmail.com', 'sdf', '2017-08-18 02:01:54');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id_news` int(3) NOT NULL,
  `title` varchar(255) NOT NULL,
  `images` varchar(500) NOT NULL,
  `description` text NOT NULL,
  `id_category` int(3) NOT NULL,
  `time_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_admin` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id_news`, `title`, `images`, `description`, `id_category`, `time_updated`, `id_admin`) VALUES
(1, 'We Are Launching Today', 'wkwk', '<p>Welcome to aezar.com.&nbsp;</p>\r\n\r\n<p>Thanks to visit us!</p>\r\n', 1, '2017-08-18 02:08:26', 0),
(4, 'Show', 'penting nggak sih join ke akselerator startup.jpg', '<p>Welcome to the show</p>\r\n', 1, '2017-08-18 02:07:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id_subscribers` int(5) NOT NULL,
  `email` varchar(200) NOT NULL,
  `time_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id_subscribers`, `email`, `time_updated`) VALUES
(1, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(2, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(3, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(4, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(5, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(6, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(7, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(8, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(9, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(10, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(11, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(12, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(13, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(14, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(15, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(16, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(17, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(18, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(20, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(21, 'tkj@gmail.com', '2017-08-13 16:22:50'),
(22, 'fadlan.kayyis@gmail.com', '2017-08-13 16:21:48'),
(23, 'hafidz@gmail.com', '2017-08-13 16:22:50'),
(25, 'developer@idmetafora.com', '2017-08-13 16:53:58'),
(26, 'jkds@gmail.com', '2017-08-13 16:54:38'),
(27, 'sdaad@fdas.cdo', '2017-08-13 16:56:10'),
(28, 'dsaf@das.dsa', '2017-08-13 16:56:26'),
(29, 'me@mahafidz.com', '2017-08-13 16:57:06'),
(30, 'hafidz.tkj@gmail.com', '2017-08-13 16:57:13'),
(31, 'ewd@dfsa.cds', '2017-08-13 16:57:18'),
(32, 'info@aezar.com', '2017-08-13 17:03:19'),
(33, 'info@arir.com', '2017-08-13 17:03:38'),
(34, 'info@aezar.com', '2017-08-13 17:05:04'),
(35, 'info@aezar.com', '2017-08-13 17:06:20'),
(36, 'info@aezar.com', '2017-08-13 17:06:26'),
(37, 'info@aezar.com', '2017-08-13 17:07:06'),
(38, 'info@aezar.com', '2017-08-13 17:10:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id_news`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id_subscribers`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id_category` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id_news` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id_subscribers` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
