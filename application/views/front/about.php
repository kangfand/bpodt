
      <!-- Page Content-->
      <main class="page-content">
        <!-- Menu-->
        <section class="bg-woodsmoke sub-menu-wrap">
          <div class="shell">
            <ul class="sub-menu">
              <li class="active"><a href="about.html">About</a></li>
              <li><a href="#">Aezar Architect</a></li>
<!--               <li><a href="publications.html">Publications</a></li>
              <li><a href="history.html">History</a></li> -->
            </ul>
          </div>
        </section>
        <!-- Who We Are-->
        <section class="section-50 section-md-90 section-lg-135">
          <div class="shell">
            <div class="range">
              <div class="cell-sm-4 text-center text-sm-left"><span class="big-mod text-primary">A</span></div>
              <div class="cell-sm-7 cell-sm-preffix-1 cell-md-8 cell-md-preffix-0">
                <h1 class="text-center text-sm-left">Tentang
<br class="hidden visible-md visible-lg">
Kami
                </h1>
                <p>
                 Aezar Architect adalah sebuah perusahaan yang bergerak pada bidang jasa perencanaan arsitektural dan desain interior. Kami memiliki team dengan kualitas SDM yang baik dengan latar belakang pengalaman di dalam dan luar negeri. Kami melayani berbagai jenis bangunan, mulai dari rumah tinggal hingga bangunan skala besar. 
                 </p>

                 <p>
                  Aezar Architect senantiasa memberikan pelayanan terbaik kepada pengguna jasa. Dengan ditunjang teknologi terbaru dalam mendesain, Aezar Architect senantiasa menghasilkan kualitas desain arsitektur yang yang lebih baik.

                </p>
              </div>
            </div>
          </div>
        </section>
        <!-- Why Choose Us-->
        <section>
          <div class="shell">
            <div class="range">
              <div class="cell-md-7">
                <div class="box-skin">
                  <h1 class="text-center text-sm-left">Visi
<br class="hidden visible-md visible-lg">
Aezar Architect
                  </h1>
                  <p>
                    Menjadi salah satu konsultan arsitek terkemuka yang dapat berkontribusi dalam bidang pembangunan di Indonesia dengan perencanaan desain berbasis teknologi terbaru .
                  </p>
                </div>
              </div>
              <div class="cell-md-5">
                <div class="box-skin-mod bg-primary">
                 <h1 class="text-center text-sm-left">Misi
<br class="hidden visible-md visible-lg">
Aezar Architect
                  </h1>
                  <blockquote class="quote">
                    <q>
                     1. Menghasilkan karya arsitektur yang berkualitas dalam setiap perencanaan desain yang dikerjakan. <br>
                      2. Memberikan pelayan terbaik kepada klien dengan memegang kepercayaan/komitmen atas waktu dan kualitas desain.<br>
                      3. Penggunaan teknologi terbaru dalam setiap proses perancangan desain arsitektur guna menghasilkan karya terbaik.<br>
                      4. Memberdayakan kreativitas, wawasan dan pengalaman kepada anggota perusahaan agar tercipta sumber daya yang berkualitas.

                    </q><span class="h6">
                      <time datetime="2014">Aezar Architect</time></span>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
        </section>


        <!-- Team-->
        <section class="section-50 section-md-70 section-lg-135 section-lg-bottom-70">
          <div class="shell">
            <div class="range text-left">

              <div class="cell-xs-12 offset-top-70 offset-lg-top-135">
                <div class="post">
                  <div class="range range-md-center">
                    <div class="cell-sm-6 cell-md-5 text-center text-sm-left">

                      <h1>Muhammad Rofiqul Fahmi</h1>
                      <h5 class="text-alto">Co Founder.</h5>
                      <ul class="list list-inline">
                        <li><a href="#" class="icon icon-gallery fa-linkedin"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-facebook"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-twitter"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-behance"></a></li>
                      </ul><img src="img/rofiqul-fahmi.jpg" width="370" height="298" alt="" class="img-responsive">
                    </div>
                    <div class="cell-sm-6 cell-md-5">
                      <p>Muhammad rofiqul Fahmi merupakan Co-founder dari Aezar Architect. Pengalaman dalam berarsitektur didapatkan ketika  masih menjadi mahasiswa dengan bekerja part time di salah satu biro arsitek di Yogyakarta.

                      </p>
                      <p> Setelah malang melintang dibeberapa  konsultan arsitek di Indonesia, Ia kemudian melanjutkan pengalaman bekerja disalah satu biro arsitek di Malaysia dan terlibat dalam beberapa proyek besar di Kuala Lumpur. Berbekal pengalaman diluar negeri, ia mendirikan Aezar Architect bersama Suryo Kurniadi dan menjadi co-founder dari Aezar Architect.</p>

                      <p>
                        <a href="<?= base_url(); ?>upload/1_Booklet Portofolio-Aezar-Architect.pdf" style="padding: 10px 20px 10px 20px; font-weight: bolder; background-color: yellow;">Download Booklet Aezar Architect</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>


        <!-- slick carousel-->
        <!-- <section class="section-70 section-md-90 section-lg-135 section-xs-bottom-115">
          <div class="shell shell-custom">
     

            <div data-arrows="true" data-for=".thumbnail-carousel" data-loop="false" data-dots="false" data-swipe="true" data-xs-items="1" class="slick-slider carousel-parent slick-slider-custom">
              <div class="item"><img src="images/archiarch-03-1170x485.jpg" alt="" width="1170" height="485"></div>
              <div class="item"><img src="images/archiarch-04-1170x485.jpg" alt="" width="1170" height="485"></div>
              <div class="item"><img src="images/archiarch-05-1170x485.jpg" alt="" width="1170" height="485"></div>
              <div class="item"><img src="images/archiarch-06-1170x485.jpg" alt="" width="1170" height="485"></div>
            </div>
            <div data-for=".carousel-parent" data-arrows="false" data-loop="false" data-dots="false" data-swipe="true" data-items="1" data-xs-items="2" data-sm-items="3" data-md-items="3" data-lg-items="3" data-slide-to-scroll="1" class="slick-slider thumbnail-carousel">
              <div class="item">
                <div><img src="images/archiarch-07-170x124.jpg" alt="" width="170" height="124"></div>
              </div>
              <div class="item">
                <div><img src="images/archiarch-08-170x124.jpg" alt="" width="170" height="124"></div>
              </div>
              <div class="item">
                <div><img src="images/archiarch-09-170x124.jpg" alt="" width="170" height="124"></div>
              </div>
              <div class="item">
                <div><img src="images/archiarch-10-170x124.jpg" alt="" width="170" height="124"></div>
              </div>
            </div>
          </div>
        </section> -->
      </main>