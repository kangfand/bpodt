      <!-- Page Content-->
      <main class="page-content">
        <!-- New York-->
        <section class="section-50 section-md-70 section-lg-135 section-md-bottom-20 section-lg-bottom-15">
          <div class="shell">
        <!--     <div class="range">

            </div> -->
            <div class="range offset-top-20 offset-md-top-15">
              <div class="cell-md-5">
                <h1 class="text-shark">Kontak</h1>
                <h5 class="text-alto offset-top-15 offset-md-top-30">Contact Us   </h5>
                <p class="small text-shark offset-md-top-5">Aezar</p>
                <a href="mailto:info@aezar.com" class="small text-shark"><span class="__cf_email__">info@aezar.com</span></a>

                <h5 class="text-alto offset-top-10 offset-md-top-30">BBM   </h5>
                <p class="small text-shark offset-md-top-5">Our PIN BBM</p><a href="#" class="small text-shark"><span class="__cf_email__">D9B826F2</span></a>
                <div class="unit unit-horizontal unit-spacing-xs unit-middle offset-top-20 offset-md-top-55">
                  <div class="unit-left"><span class="icon material-design-phone370 icon-primary icon-lg"></span></div>
                  <div class="unit-body">
                    <p class="small text-shark">Call Us Toll Free:</p><a href="callto:08122-651-2222" class="font-sec big text-primary">+62 8122-651-2222</a>
                  </div>
                </div>
                <div class="unit unit-horizontal unit-middle offset-top-20">
                  <div class="unit-left"><span class="icon material-design-map103 icon-primary icon-lg"></span></div>
                  <div class="unit-body"><a href="#" class="small text-shark text-height-25 address-info"><span class="text-height-25">Krapayak Kulon, 205 / 07, Panggungharjo, 
                  <br> 
                  Sewon, Bantul, Yogyakarta</span></a></div>
                </div>
              </div>
              <div class="cell-md-7">
                <!-- RD Google Map-->
               
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d988.1621074313252!2d110.36192378055449!3d-7.826984126649413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a57ba06007085%3A0x3ee22892928fb21!2sPondok+Pesantren+Al-Munawwir!5e0!3m2!1sen!2sid!4v1503018442960" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                  <ul class="map_locations">
                    <li data-x="-73.9874068" data-y="40.643180">
                      <p>Krapayak Kulon, 205 / 07, Panggungharjo, 
                  <br> 
                  Sewon, Bantul, Yogyakarta</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section-bottom-50 section-md-bottom-70 section-lg-bottom-135">
          <div class="shell">
            <form data-form-output="form-output-global" data-form-type="contact" method="post" action="<?= base_url(); ?>contacts/add" class="range rd-s text-left rd-mailform-contact">
              <div class="cell-md-4 cell-lg-5 cell-xs-12 offset-md-top--35">
                <!-- <h1 class="text-shark">Contacts</h1> -->
                <h1 class="text-shark">Karir</h1>
                <p>Aezar Architect membuka kesempatan bagi anda untuk bergabung bersama team kami. Kami memerlukan staff untuk mengisi posisi Arsitek, 3D Arstis dan Drafter.</p>

                <p> Jika anda tertarik bergabung, silahkan kirimkan portofolio terbaik anda melalui email yang tertera. Terimakasih</p>
              </div>
              <div class="cell-md-4 cell-lg-3 cell-sm-6 offset-top-70 offset-md-top-0">
                <div class="form-group">
                  <label for="contact-name" class="form-label">Name:</label>
                  <input id="contact-name" type="text" name="name" data-constraints="@Required" class="form-control">
                </div>
                <div class="form-group">
                  <label for="contact-phone" class="form-label">Company:</label>
                  <input id="contact-phone" type="text" name="company" data-constraints="@Required" class="form-control">
                </div>
                <div class="form-group">
                  <label for="contact-email-1" class="form-label">E-Mail:</label>
                  <input id="contact-email-1" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                </div>
              </div>
              <div class="cell-md-4 cell-lg-4 cell-sm-6 offset-top-70 offset-md-top-0">
                <div class="form-group">
                  <label for="contact-message" class="form-label">Your Message:</label>
                  <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control"></textarea>
                </div>
              </div>
              <div class="cell-xs-12 text-sm-right offset-top-30">
                <button type="submit" class="btn btn-default">Submit</button>
              </div>
            </form>
          </div>
        </section>
      </main>