      <!-- Page Content-->
      <main class="page-content">
        <!-- New York-->
        <section class="section-35 section-md-35 section-lg-50 section-md-bottom-20 section-lg-bottom-15">


        </section>
        <section id class="section-bottom-20 section-md-bottom-70 section-lg-bottom-35">
          <div class="shell">
            <div class="range rd-mailform text-left rd-mailform-contact">
           
            <?php if($results<1) 
              {
                echo "<p>Data tidak tersedia.</p>";
              }

              else 
                { 
              foreach ($results as $data): ?>
              <div class="cell-md-8 cell-lg-9 cell-sm-6 offset-top-70 offset-md-top-0">
              <h1><?= $data->title; ?></h1>
               <small>
              <i style="width: 29px; margin: 3px;" href="#" class="icon fa-calendar">  </i>
              <font href="#" style=" padding-bottom: 0px; margin: 0px;"><?= substr($data->time_updated, 0, 11); ?>  </font>
               </small>
              <p><img style="width: 90%;" src="<?= base_url(); ?>upload/<?= $data->images; ?>">
              </p>
              
              <p><?= $data->description; ?>
              </p>    
              </div>

            <?php endforeach; } ?>



                 <div class="cell-md-4 cell-lg-3 cell-xs-12 offset-md-top--35">
                <!-- <h1 class="text-shark">Contacts</h1> -->
                <h1 class="text-shark">Latest News</h1>
                <div id="navbar">
                  <ul>
                    <?php foreach ($sidebar as $list): ?>
                    <li><a href="<?= base_url(); ?>news/detail/<?= $list->id_news ?>/<?= url_title($list->title); ?>">
                    <?= $list->title; ?>                                       
                    </a></li>
                  <?php endforeach; ?>
                  </ul>
                </div>
              </div>
          

            </div>
          </div>
        </section>
      </main>