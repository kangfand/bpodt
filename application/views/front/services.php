
      <!-- Page Content-->
      <main class="page-content">
        <!-- Menu-->
        <section class="bg-woodsmoke sub-menu-wrap">
          <div class="shell">
            <ul class="sub-menu">
              <li class="active"><a href="about.html">About</a></li>
              <li><a href="awards.html">Awards</a></li>
<!--               <li><a href="publications.html">Publications</a></li>
              <li><a href="history.html">History</a></li> -->
            </ul>
          </div>
        </section>
        <!-- Who We Are-->
        <section class="section-50 section-md-90 section-lg-135">
          <div class="shell">
            <div class="range">
              <div class="cell-sm-4 text-center text-sm-left"><span class="big-mod text-primary">L</span></div>
              <div class="cell-sm-7 cell-sm-preffix-1 cell-md-8 cell-md-preffix-0">
                <h1 class="text-center text-sm-left">Layanan
<br class="hidden visible-md visible-lg">
Kami
                </h1>
                <p>
                 <stong>Aezar Architect</stong> melayani berbagai macam jenis bangunan mulai dari bangunan skala kecil hingga skala kawasan. kami didukung oleh tenaga staff yang profesional dengan pengalaman kerja didalam maupun diluar negeri. Staff yang kami miliki memiliki etos kerja yang tinggi serta attitude yang baik dalam bekerja sehingga pelayanan yang diberikan kepada anda sangat maksimal .
                 </p>

<br>
                 <h1>Lingkup Desain</h1>
                 <p>
                 Rumah Tinggal, Rumah Kos, Villa, guest house, resort, Perumahan, apartemen, hotel , hostel, condotel, spa, restoran, kantor, ruko, rukan, mall, pasar modern, ruang pameran, sekolah, galeri, museum, kampus, Bangunan rohani Dll.

                </p>
<br>
                <h1>Gambar Yang Diperoleh</h1><br>
<h2>Gambar Arsitektur</h2>
<p>
1 . Denah <br>
2. Siteplan<br>
3. Tampak Depan<br>
4 . Tampak Samping Kanan<br>
5 . Tampak Samping Kiri<br>
6 . Tampak Belakang<br>
7 . Potongan A-A<br>
8 . Potongan B-B Dst.<br>
9 . Rencana Kusen Pintu dan Jendela<br>
10. Detail Kusen Pintu dan Jendela<br>
11 . Rencana Plafond<br>
12 . Detail Fasad<br>
13 . Detail Komponen Arsitektural<br>
</p>








              </div>
            </div>
          </div>
        </section>
        <!-- Why Choose Us-->
        <section>
          <div class="shell">
            <div class="range">
              <div class="cell-md-7">
                <div class="box-skin">
                  <h1 class="text-center text-sm-left">Gambar
<br class="hidden visible-md visible-lg">
MEP
                  </h1>
                  <p>

1 . Rencana Instalasi Air Bersih<br>
2 . Rencana Instalasi Air Bekas<br>
3 . Rencana Instalasi Air Kotor<br>
4 . Rencana Instalasi Air Hujan<br>
5. Rencana Instalasi Listrik<br>
6. Rencana Utilitas<br>
                  </p>
                </div>
              </div>
              <div class="cell-md-5">
                <div class="box-skin-mod bg-primary">
                 <h1 class="text-center text-sm-left">Gambar
<br class="hidden visible-md visible-lg">
Struktur
                  </h1>
                  <blockquote class="quote">
                    <q>
                     <br>

1 . Rencana Pondasi<br>
2 . Detail Pondasi<br>
3 . Rencana Slof<br>
4 . Rencana Kolom<br>
5 . Rencana Balok<br>
6 . Rencana Atap<br>
7 . Detail Atap<br>

<br>
<h1>Gambar Presentasi</h1><br>

1 . 3D View (Render)<br>
2 . 3D View (Sketcup)<br>
3. Lumion (Opsional)<br>
<br>
<h3>Gambar Rencana Anggaran Biaya dan Dokumen IMB</h3>
<br>
1 . RAB Standar<br>
2. Dokumen IMB Lengkap<br>

                    </q><span class="h6">
                      <time datetime="2014">Aezar Architect</time></span>
                  </blockquote>
                </div>
              </div>
            </div>
          </div>
        </section>




       

      </main>