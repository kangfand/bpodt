
      <!-- Page Content-->
      <main class="page-content text-center">
        <!-- Menu-->
        <section class="bg-woodsmoke sub-menu-wrap">
          <div class="shell">
            <ul class="sub-menu sub-menu-1">
              <li class="active"><a href="teams.html">Founder</a></li>
             <li><a href="#director">Director</a></li>
              <li><a href="#co-founder">Architect</a></li>
                 <!--   <li><a href="landscape-architect.html">Landscape architect</a></li>
              <li><a href="interior.html">Interior architect</a></li>
              <li><a href="careers.html">Careers</a></li> -->
            </ul>
          </div>
        </section>
        <!-- Team-->
        <section class="section-50 section-md-70 section-lg-135 section-lg-bottom-70">
          <div class="shell">
            <div class="range text-left">
              <div class="cell-xs-12">
                <div class="post">
                  <div class="range range-md-center">
                    <div class="cell-sm-6 cell-md-5 text-center text-sm-left">
                      <h1 id="#director">Suryo Kurniadi</h1>
                      <h5 class="text-alto">Founder</h5>
                      <ul class="list list-inline">
                        <li><a href="#" class="icon icon-gallery fa-linkedin"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-facebook"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-twitter"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-behance"></a></li>
                      </ul><img src="img/suryo-kurniadi.jpg" width="370" height="298" alt="" class="img-responsive">
                    </div>
                    <div class="cell-sm-6 cell-md-5">
                      <p>Suryo Kurniadi merupakan Founder dan Direktur dari Aezar  Architect. Pengalaman dalam berarsitektur didapatkan ketika  masih menjadi mahasiswa dengan terlibat dalam berbagai proyek  yang dimiliki dosen. Mulai dari bangunan sederhana hingga  pemetaan tatakota di beberapa kota di Indonesia.
                      </p>
                      <p> Setelah  menyelesaikan studi di UTY, memperdalam pengalaman dalam  berarsitektur dengan bekerja di beberapa konsultan arsitek.  Berbekal pengalaman yang dimiliki, Suryo Kurniadi mendirikan  sebuah biro arsitek yang diberi nama Aezar Architect</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="cell-xs-12 offset-top-70 offset-lg-top-135">
                <div class="post">
                  <div class="range range-md-center">
                    <div class="cell-sm-6 cell-md-5 text-center text-sm-left">

                      <h1>Muhammad Rofiqul Fahmi</h1>
                      <h5 class="text-alto">Co Founder.</h5>
                      <ul class="list list-inline">
                        <li><a href="#" class="icon icon-gallery fa-linkedin"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-facebook"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-twitter"></a></li>
                        <li><a href="#" class="icon icon-gallery fa-behance"></a></li>
                      </ul><img src="img/rofiqul-fahmi.jpg" width="370" height="298" alt="" class="img-responsive">
                    </div>
                    <div class="cell-sm-6 cell-md-5">
                      <p>Muhammad rofiqul Fahmi merupakan Co-founder dari Aezar Architect. Pengalaman dalam berarsitektur didapatkan ketika  masih menjadi mahasiswa dengan bekerja part time di salah satu biro arsitek di Yogyakarta.

                      </p>
                      <p> Setelah malang melintang dibeberapa  konsultan arsitek di Indonesia, Ia kemudian melanjutkan pengalaman bekerja disalah satu biro arsitek di Malaysia dan terlibat dalam beberapa proyek besar di Kuala Lumpur. Berbekal pengalaman diluar negeri, ia mendirikan Aezar Architect bersama Suryo Kurniadi dan menjadi co-founder dari Aezar Architect.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- Need Bright Heads!-->
        <section class="section-50 section-md-70 section-lg-135">
          <div class="shell">
            <div class="range text-left">
              <div class="cell-md-7 cell-sm-6">
                <div class="product">
                  <h1>Need Bright<br>Heads!</h1>
                  <h5 class="text-alto inset-lg-right-130 inset-md-right-80">
                    Vacancy! Do you want to work here?
                    See our careers topic!
                  </h5><a href="#" class="banner banner-team-05-253-67"></a>
                </div>
              </div>
              <div class="cell-md-5 cell-sm-6">
                <div class="box-skin-mod-1 box-skin-mod-1-right">
                  <div class="box-skin-mod-1-inner box-skin-mod-1-inner-1"><img src="images/team-07-255-254.png" width="255" height="254" alt="" class="img-responsive"></div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>