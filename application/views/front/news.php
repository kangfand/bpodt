      <!-- Page Content-->
      <main class="page-content">
        <!-- New York-->
<!--         <section class="section-35 section-md-35 section-lg-50 section-md-bottom-20 section-lg-bottom-15">


        </section> -->

<br><br>
<div class="container">
      <div class="col-8 col-sm-8">
                    <?php if($results<1) 
              {
                echo "<p>Data tidak tersedia.</p>";
              }

              else 
                { 
              foreach ($results as $data): 
                ?>
              <div class="cell-md-8 cell-lg-9 cell-sm-6 offset-top-70 offset-md-top-0">
              <h1><a href="<?= base_url(); ?>news/detail/<?= $data->id_news; ?>/<?= url_title($data->title); ?>">
              <?= $data->title; ?></a></h1> 
              <small>
              <i style="width: 29px; margin: 3px;" href="#" class="icon fa-calendar">  </i>
              <font href="#" style=" padding-bottom: 0px; margin: 0px;"><?= substr($data->time_updated, 0, 11); ?>  </font>
               </small>
              <p><img style="width: 90%;" src="<?= base_url(); ?>upload/<?= $data->images; ?>">
              </p>              
              <p><?= $data->description; ?>
              </p>    
              </div>


            <?php endforeach; } ?>

            <center><?= $links ?></center>



      </div>
      <div class="col-4 col-sm-4">
        <h1 class="text-shark">Latest News</h1>
                <div id="navbar">
                  <ul>
                    <?php foreach ($sidebar as $list): ?>
                    <li><a href="<?= base_url(); ?>news/detail/<?= $list->id_news ?>/<?= url_title($list->title); ?>">
                    <?= $list->title; ?>                                       
                    </a></li>
                  <?php endforeach; ?>
                  </ul>
                </div>
      </div>
</div>



  

      </main>