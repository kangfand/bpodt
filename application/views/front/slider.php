
      <!-- Page Contentv-->
      <main class="page-content text-center page-content-mod">
        <!---->
        <section>
          <!-- Swiper-->
          <div data-height="100vh" data-min-height="550px" data-slide-effect="fade" data-loop="true" data-autoplay="false" class="swiper-container swiper-slider swiper-custom">
            <div class="swiper-wrapper">


              <?php 
                if($results<1) 
                {
                  ?>        
 <?php
                }

                else { 
                  foreach ($results as $data): ?> 

              <div data-slide-bg="<?= base_url(); ?>upload/<?= $data->images_cover; ?>" data-slide-title="Alam Hostel" class="swiper-slide">
                <div class="swiper-slide-caption">
                  <div class="shell">
                    <div class="range range-xs-center">
                      <div class="cell-md-10 cell-sm-9">
                        <div class="box-info goal">
                          <div class="box-info-head">
                        <!--      <h1 class="text-center">

                            <?= $data->name; ?>

                            </h1>
                           <div class="button">
                              <button data-custom-toggle=".goal" data-custom-toggle-disable-on-blur="true" class="btn btn-white">More Info</button>
                            </div> -->
                          </div>
                          <div class="box-info-caption text-sm-left">
                            <h1 class="text-center hidden-md hidden-lg">

                

                            </h1>
                            <div class="range">
                              <div class="cell-sm-4">
                                <div class="content">

                                </div>
                              </div>
                              <div class="cell-sm-4 offset-top-0">
                                <div class="content">

                                </div>
                              </div>
                              <div class="cell-sm-4 offset-top-0">
                                <div class="content">

                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

                 <?php endforeach;
          } ?>


            </div>
            <!-- Swiper Pagination-->
            <div class="swiper-pagination" style="float:right; margin-right:auto; margin-left: auto; text-align: center;"></div>
            <!-- Swiper Navigation-->
            <div class="swiper-button swiper-button-prev"><span class="swiper-button__arrow"></span>
              <div class="preview">
                <h3 class="title"></h3>
                <div class="preview__img"></div>
              </div>
            </div>
            <div class="swiper-button swiper-button-next"><span class="swiper-button__arrow"></span>
              <div class="preview">
                <h3 class="title"></h3>
                <div class="preview__img"></div>
              </div>
            </div>
          </div>
        </section>
      </main>