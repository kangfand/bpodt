

      <!-- Page Contentv-->
      <main class="page-content text-center page-content-mod">
        <!-- Swiper -->
        <section class="section-50 section-md-70 section-lg-top-135">
          <!-- Swiper-->
          <div class="shell">
            <div data-height="640px" data-min-height="400px" class="swiper-container swiper-slider swiper-mod">
              <div class="swiper-wrapper">
                <div data-slide-bg="images/slide-02.jpg" class="swiper-slide">
                  <div class="swiper-slide-caption"></div>
                </div>
                <div data-slide-bg="images/slide-03.jpg" class="swiper-slide">
                  <div class="swiper-slide-caption"></div>
                </div>
                <div data-slide-bg="images/slide-01.jpg" class="swiper-slide">
                  <div class="swiper-slide-caption"></div>
                </div>
              </div>
              <!-- Swiper Pagination-->
              <div class="swiper-pagination"></div>
              <!-- Swiper Navigation-->
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>
            </div>
          </div>
        </section>
        <!-- Alexandria House-->
        <section class="section-70">
          <div class="shell">
            <h1 class="text-md-left text-shark">Alexandria<br>House</h1>
            <div class="range text-left offset-sm-top-35 offset-md-top-85">
              <div class="cell-md-3 cell-sm-6">
                <dl class="list-terms-custom">
                  <dt class="h5 text-alto">Location</dt>
                  <dd class="small text-shark">Alexandria, VA, USA</dd>
                  <dt class="h5 text-alto">General Square</dt>
                  <dd class="small text-shark">5 000 sq. ft.</dd>
                  <dt class="h5 text-alto">Category</dt>
                  <dd class="small text-shark">Housing</dd>
                  <dt class="h5 text-alto">Design Team</dt>
                  <dd class="small text-shark">Gail Lanson,<br>Sam Wilson</dd>
                  <dt class="h5 text-alto">Years</dt>
                  <dd class="small text-shark">2011 (Competition) –<br>2015 (Completion)</dd>
                </dl>
              </div>
              <div class="cell-md-3 cell-sm-6">
                <dl class="list-terms-custom">
                  <dt class="h5 text-alto">Awards</dt>
                  <dd class="small text-shark">Open competition
<br class="hidden visible-md visible-lg">
1st prize
                  </dd>
                  <dt class="h5 text-alto">Architect in Charge</dt>
                  <dd class="small text-shark">Kate Hudson</dd>
                  <dt class="h5 text-alto">Client</dt>
                  <dd class="small text-shark">TrustIndastr Inc.</dd>
                  <dt class="h5 text-alto">Project Architect</dt>
                  <dd class="small text-shark">Jan Anderson</dd>
                </dl>
              </div>
              <div class="cell-md-6">
                <p>The Alexandria House project is based on the winning entry for an open competition organized by the condominium cooperative, asking to design a building intended to serve as an identifiable landmark for the sprawling suburb of Alexandria. The proposal transforms the existing community centre into a cloister-like structure clad entirely in deep, rich copper, which over time will turn a vivid shade of green.</p>
                <p>Externally the building stands out in its compact mass. The interior space with its white, bright surfaces, wooden ceilings and movable panels stands in stark contrast to the almost monolithic appearance of the exterior. Strip-like windows cut into the concrete structure of the walls and of the roof, generously letting natural light into the space and creating a visual connection with the surrounding landscape.</p>
                <p>The building serves multiple functions. In addition to the church nave, there is a columbarium, a day care centre, a youth centre and a meeting place for the scouts, office spaces and rooms for priests and social workers to meet with members of the community.</p>
                <p>The brick facades of the existing building are plastered in an intense red color that stands in contrast to the dark reddish and greenish patina of the copper of the church facades.</p>
              </div>
<!--               <div class="cell-xl-12 offset-md-top--50">
                <ul class="list list-inline list-inline list-inline-md">
                  <li><a href="#" class="icon icon-gallery fa-linkedin"></a></li>
                  <li><a href="#" class="icon icon-gallery fa-dribbble"></a></li>
                  <li><a href="#" class="icon icon-gallery fa-facebook"></a></li>
                  <li><a href="#" class="icon icon-gallery fa-instagram"></a></li>
                  <li><a href="#" class="icon icon-gallery fa-twitter"></a></li>
                  <li><a href="#" class="icon icon-gallery fa-behance"></a></li>
                </ul>
              </div> -->
            </div>
          </div>
        </section>


        <!-- Comments-->
        <!-- <section class="section-50 section-md-70 section-lg-135"> -->
          <!-- <div class="shell">
            <div class="range range-xs-reverse range-xs-middle">
              <div class="cell-xs-6">
                <div class="input-group-mod"><img src="images/alexandria-house-20x19.png" width="20" height="19" alt="">
                  <div class="form-group form-group-mod">
                    

                    <select data-placeholder="Select an option" data-minimum-results-for-search="Infinity" class="form-control select-filter">
                      <option>Login</option>
                      <option> Google</option>
                      <option> Twitter</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="cell-xs-6 text-xs-left offset-xs-top-0 offset-top-10">
                <h6 class="font-sec-1 baltica-sea heading-post">2 Comments</h6>
              </div>
            </div>
            <hr class="hr-xs hr-small">
            <div class="range range-xs-middle offset-xs-top-0 offset-top-20">
              <div class="cell-xs-6">
                <div class="form-group form-group-mod-1">
                  
                  
                  <select data-placeholder="Select an option" data-minimum-results-for-search="Infinity" class="form-control select-filter">
                    <option>Sort By Best</option>
                    <option>Reply</option>
                    <option>Days</option>
                  </select>
                </div>
              </div>
              <div class="cell-xs-6 text-xs-right offset-top-0">
                <ul class="list-inline list-inline-md">
                  <li><a href="#" class="text-shark small-mod-2 text-medium">Share<span class="icon material-design-share39 icon-xs icon-shark"></span></a></li>
                  <li><a href="#" class="text-shark small-mod-2 text-medium">Favorite<span class="icon material-design-mark1 icon-xs icon-shark"></span></a></li>
                </ul>
              </div>
            </div>
            <div class="range text-xs-left text-center offset-top-30">
              <div class="cell-xs-3 cell-lg-1 cell-sm-2"><img src="images/alexandria-house-02-69x69.jpg" width="69" height="69" alt="" class="img-responsive"></div>
              <div class="cell-xs-9 cell-lg-11 cell-sm-10 offset-xs-top-0 offset-top-15">
                <div class="form-group form-group-mod-2">
                  <label for="contact-message" class="form-label">Join the discussion...</label>
                  <textarea id="contact-message" name="message" data-constraints="@Required" class="form-control"></textarea>
                </div>
              </div>
            </div> -->

            <!-- 
            <div class="range text-xs-left text-center offset-top-0">
              <div class="cell-xs-3 cell-lg-1 cell-sm-2"><img src="images/alexandria-house-03-69x69.jpg" width="69" height="69" alt="" class="img-responsive offset-md-top-10"></div>
              <div class="cell-xs-9 cell-lg-11 cell-sm-10 offset-xs-top-0 offset-top-15">
                <div class="preffix-xs-left--20 preffix-lg-left-0 preffix-md-left--50"><span class="small-mod-2 baltica-sea text-medium">Sarah Williams</span><span class="silver-chalice small-mod-3 text-regular preffix-left-25">15 hours ago</span>
                  <p class="small-mod-3 text-regular bombay offset-top-12">Is it those building that Francis Corra discuss during WWAC?</p>
                  <div class="post-wrapper"><span class="small-mod-1 baltica-sea">4</span><span class="material-design-drop25 icon icon-xs icon-silver-chalice icon-middle"></span><span class="material-design-drop27 icon icon-xs icon-silver-chalice icon-middle"></span>
                    <ul class="list-inline list-inline-lg list-inline-block">
                      <li><a href="#" class="text-shark small-mod-2 text-medium">Reply<span class="icon material-design-underline6 icon-xxs icon-shark"></span></a></li>
                      <li><a href="#" class="text-shark small-mod-2 text-medium">Share<span class="icon material-design-share39 icon-xxs icon-shark"></span></a></li>
                    </ul>
                  </div>
                </div>
                <hr class="hr-sm preffix-xs-left--20 preffix-lg-left-0 preffix-md-left--50 hr-small">
              </div>
            </div> -->

            <!-- 
            <div class="range text-xs-left text-center offset-top-0">
              <div class="cell-xs-3 cell-lg-1 cell-sm-2 cell-lg-preffix-1 cell-sm-preffix-1"><img src="images/alexandria-house-03-69x69.jpg" width="69" height="69" alt="" class="img-responsive offset-md-top-10"></div>
              <div class="cell-xs-9 cell-lg-10 cell-sm-9 offset-xs-top-0 offset-top-15">
                <div class="preffix-xs-left--20 preffix-lg-left-0 preffix-md-left--50"><span class="small-mod-2 baltica-sea text-medium">Set Rogers</span><a href="#" class="preffix-left-25 bombay"><span class="icon material-design-forward18 icon-xxs bombay"></span><span class="small-mod-2 text-medium bombay preffix-left-5">Sarah Williams</span></a><span class="silver-chalice small-mod-3 text-regular preffix-left-30">15 hours ago</span>
                  <p class="small-mod-3 text-regular bombay offset-top-12">You are right.</p>
                  <div class="post-wrapper"><span class="small-mod-1 baltica-sea">4</span><span class="material-design-drop25 icon icon-xs icon-silver-chalice icon-middle"></span><span class="material-design-drop27 icon icon-xs icon-silver-chalice icon-middle"></span>
                    <ul class="list-inline list-inline-lg list-inline-block">
                      <li><a href="#" class="text-shark small-mod-2 text-medium">Reply<span class="icon material-design-underline6 icon-xxs icon-shark"></span></a></li>
                      <li><a href="#" class="text-shark small-mod-2 text-medium">Share<span class="icon material-design-share39 icon-xxs icon-shark"></span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div> -->

     <!--      </div>
        </section> -->
      </main>
    
