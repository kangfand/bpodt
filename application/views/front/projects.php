
      <!-- Page Contentv-->
      <main class="page-content text-center page-content-mod">
        <!-- Swiper-->
<!--         <div data-height="100vh" data-min-height="400px" data-slide-effect="fade" data-loop="true" data-autoplay="false" class="swiper-container swiper-slider swiper-custom"> -->
     <!--      <div class="swiper-wrapper">
            
          </div> -->
          <!-- Swiper Pagination-->
          <div class="swiper-pagination"></div>
          <!-- Swiper Navigation-->
          <div class="swiper-button swiper-button-prev"><span class="swiper-button__arrow"></span>
            <div class="preview">
              <h3 class="title"></h3>
              <div class="preview__img"></div>
            </div>
          </div>
          <div class="swiper-button swiper-button-next"><span class="swiper-button__arrow"></span>
            <div class="preview">
              <h3 class="title"></h3>
              <div class="preview__img"></div>
            </div>
          </div><a href="#" data-waypoint-to="#portfolio" class="waypoint"><span>Aezar Portfolio</span></a>
    <!--     </div> -->
        <!-- Portfolio-->
        <section id="portfolio" class="section-70 section-lg-135">
          <div class="container">
            <h1 data-wow-delay=".3s" class="text-shark wow fadeIn"> Aezar<br>Portfolio</h1>
            <div class="row">

              <!-- Isotope Filters-->

              <div class="col-lg-12">
                <div class="row offset-md-top-35">
                  <div class="col-md-8 col-lg-6 text-md-left col-xs-12 cell-sm-12">
                    <div class="isotope-filters isotope-filters-horizontal offset-lg-top-35">
                      <ul class="list-inline list-inline-lg">
                        <li data-wow-delay=".3s" class="wow fadeInRight"><a data-isotope-filter="*" data-isotope-group="gallery" href="#" class="active">All</a></li>

                        <?php foreach ($building as $cat): ?>

                        <li data-wow-delay=".35s" class="wow fadeInRight"><a data-isotope-filter="<?= $cat->building ?>" data-isotope-group="gallery" href="#"><?= $cat->building ?></a></li>

                      <?php endforeach; ?>

                    <!--     <li data-wow-delay=".4s" class="wow fadeInRight"><a data-isotope-filter="Category 2" data-isotope-group="gallery" href="#">Category 2</a></li>
                        <li data-wow-delay=".45s" class="wow fadeInRight"><a data-isotope-filter="Category 3" data-isotope-group="gallery" href="#">Category 3</a></li> -->
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-4 col-lg-6 text-md-right col-xs-12 cell-sm-12">
                    <div class="form-group form-group-custom">
                      <label for="price"></label>
                      <select id="price" title="Choose one of the following..." class="form-control selectpicker rd-mailform-select"><!-- 
                        <option value="" label=" ">Filter</option> -->
                        <option>Portfolio</option>
                      </select>
                    </div>
                  </div>
                </div>
                <hr class="divider hidden visible-md visible-lg">
              </div>
              <div>

                <div data-isotope-layout="masonry" data-isotope-group="gallery" class="isotope">
                  <div class="row">

              <?php
              if($results<1){
                echo "<p>Belum ada project yang di upload</p>";
              }
              else 
              {
               foreach ($results as $data):  ?>

                   <div data-filter="<?= $data->building; ?>" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                      <div class="img-thumbnail-custom text-md-left"><img src="<?= base_url(); ?>upload/<?= $data->images_cover ?>" width="370" height="370" alt="" class="img-responsive">
                        <div class="img-thumbnail-custom-caption">
                          <div class="img-thumbnail-custom-caption-title">
                            <h1 class="text-white"><?= $data->name; ?>
              
              
<br>

                            </h1>
                            <h5 class="text-primary"><?= $data->building; ?></h5>
                          </div>
                          <div class="img-thumbnail-custom-caption-foot">
                          <a href="<?= base_url(); ?>projects/detail/<?= $data->id_portfolio; ?>" class="btn btn-white">More Info</a></div>
                        </div>
                      </div>
                    </div>

                <?php  endforeach; } ?>


                    <!-- <div data-filter="Category 2" class="col-xs-12 col-sm-6 col-md-4 isotope-item">
                      <div class="img-thumbnail-custom text-md-left"><img src="images/projects-09-370x370.jpg" width="370" height="370" alt="" class="img-responsive">
                        <div class="img-thumbnail-custom-caption">
                          <div class="img-thumbnail-custom-caption-title">
                            <h1 class="text-white">Alexandria
<br>
Villa
                            </h1>
                            <h5 class="text-primary">Category 2</h5>
                          </div>
                          <div class="img-thumbnail-custom-caption-foot"><a href="product.html" class="btn btn-white">More Info</a></div>
                        </div>
                      </div>
                    </div> -->

                  </div>
                </div>
              </div>
            </div><a href="projects.html" class="btn btn-default">View More Projects</a>
          </div>
        </section>
      </main>