  <!-- Page Contentv-->
      <main class="page-content text-center page-content-mod">
         <section class="section-70">
          <div class="shell">

                  <?php 
                if($results<1) 
                {
                  echo "<p>Data tidak tersedia!</p>";
                }

                else { 
                  foreach ($results as $data): ?>
            <h1 class="text-md-left text-shark"><?= $data->name; ?></h1>


            <div class="range text-left" style="border: solid 0px red;">
            


          <div class="cell-md-6 cell-sm-6" style="border: solid 0px green;"> 
                  <div class="cell-md-3 cell-sm-6" style="border: solid 0px blue; margin-right: 50px; float: left;">
                  
                    <dl class="list-terms-custom">
            
                      <dt class="h5 text-alto">Location</dt>
                      <dd class="small text-shark"><?= $data->location; ?></dd>
                      <dt class="h5 text-alto">General Square</dt>
                      <dd class="small text-shark"><?= $data->area; ?> M2</dd>
                      <dt class="h5 text-alto">Category</dt>
                      <dd class="small text-shark"><?= $data->building; ?></dd>
                      <dt class="h5 text-alto">Constructor</dt>
                      <dd class="small text-shark"><?= $data->constructor; ?></dd>
                      <dt class="h5 text-alto">Years</dt>
                      <dd class="small text-shark"><?= $data->year; ?> </dd>


                    </dl>
                  </div>

                    <div class="cell-md-12 cell-sm-12" style=" border: solid 0px orange; float: left;">
                      <dl class="list-terms-custom">
                        <dt class="h5 text-alto">Estimator</dt>
                        <dd class="small text-shark"><?= $data->estimator; ?>
      <!-- <br class="hidden visible-md visible-lg">
      1st prize -->
                        </dd>
                        <dt class="h5 text-alto">Architect </dt>
                        <dd class="small text-shark"><?= $data->architect; ?></dd>
                        <dt class="h5 text-alto">Client</dt>
                        <dd class="small text-shark"><?= $data->client; ?></dd>
                        <dt class="h5 text-alto">Drafter</dt>
                        <dd class="small text-shark"><?= $data->drafter; ?></dd>
                        <dt class="h5 text-alto">Visualizer</dt>
                        <dd class="small text-shark"><?= $data->visualizer; ?></dd>
                      </dl>

                        <?php endforeach;
                       } 
                      ?>


                    </div>



                  <div class="cell-md-3 cell-sm-6" style="border: solid 0px orange; clear: both;">
                  <br>
                  <h4>Other Portfolio</h4>
                  <style type="text/css">
                    .other-portfolio{
                      border: solid 1px #ccc; width: 160px; height: 160px; margin:5px;
                    }
                    .img-ico{
                      width: 28px;
                      padding: 3px;
                      margin:3px;
                    }
                  </style>

                <?php 
                if($other<1) 
                {
                  echo "<p>Data tidak tersedia!</p>";
                }

                else { 
                  foreach ($other as $data_o): ?>
                  <a href="" title="<?= $data_o->name; ?>">
                  <img src="<?= base_url(); ?>upload/<?= $data_o->images_cover; ?>" class="other-portfolio" />
                  </a>

                <?php endforeach; } ?>



                  </div>


                  <div class="cell-md-3 cell-sm-6" style="border: solid 0px orange; clear: both;">
                  <br><br>
                  <!-- <h4>Office</h4> -->
                    <dl class="list-terms-custom">
            
                      <dt class="h5 text-alto"><img src="<?= base_url() ?>img/office.png" class="img-ico">Office</dt>
                      <dd class="small text-shark">Krapayak Kulon, 205/07, Panggungharjo, Sewon, Bantul, Yogyakarta </dd>

                      <dt class="h5 text-alto"><img src="<?= base_url() ?>img/phone.png" class="img-ico">
                      Phone</dt>
                      <dd class="small text-shark">08122-651-2222 </dd>

                      <dt class="h5 text-alto"><img src="<?= base_url() ?>img/email.png" class="img-ico">
                      Email</dt>
                      <dd class="small text-shark">info@aezar.com </dd>

                      <dt class="h5 text-alto"><img src="<?= base_url() ?>img/web.png" class="img-ico">Website</dt>
                      <dd class="small text-shark">
                      <a href="http://aezar.com">www.aezar.com</a>
                      </dd>
                      <dt class="h5 text-alto"><img src="<?= base_url() ?>img/bbm.png" class="img-ico">BBM</dt>
                      <dd class="small text-shark">D9B826F2 </dd>

                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d988.1621074313252!2d110.36192378055449!3d-7.826984126649413!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a57ba06007085%3A0x3ee22892928fb21!2sPondok+Pesantren+Al-Munawwir!5e0!3m2!1sen!2sid!4v1503018442960" width="500" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                      </dl>
                  </div>


                    </div>
              
              </p>



 












  



              <div class="cell-md-6">
                <p>

              <?php 
                if($detail_images<1) 
                {
                  ?>        
                <?php
                }

                else { 
                  foreach ($detail_images as $datai): ?>                        
                <img alt="Jasa Arsitek Yogyakarta Indonesia" src="<?= base_url(); ?>upload/<?= $datai->images; ?>" width="100%" />
                  <br> <br>  
                <?php endforeach; } ?>

                </p>

              </div> 


          </div>
          </div>
        </section>



       

      </main>
    
