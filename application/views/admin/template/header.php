<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title><?= $title ?> - Danau Toba</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="<?= base_url(); ?>admin/css/normalize.css">
  <link rel="stylesheet" href="<?= base_url(); ?>admin/css/skeleton.css">
  <link rel="stylesheet" href="<?= base_url(); ?>admin/css/backend.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

<?php 
    $username = $this->session->userdata('username');
    $password = $this->session->userdata('password');
    if($username==true)
      {
        //echo "anda berhak mengakses folder ini";
      }
    else
      {
          redirect('login', true); 
      }


?>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

  <header>
      <div class="two columns" id="logo">
        <h1>BPODT</h1>        
      </div>

      <div class="eight columns">
        <h2>Danau Toba</h2>
      </div>


      <div class="two columns">
        <h2><?php echo anchor('Logout', 'Logout') ?> <small></small></h2>        
      </div>
    
  </header>
