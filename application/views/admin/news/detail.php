 <!-- show content space -->
  
          <h3 class="title-content"> Data News &nbsp; &nbsp; &nbsp; 
          <a href="<?= base_url('admin/news/add'); ?>" class="button">Add News</a></h3>


          <table>
            <thead>
              <tr>
                <td>Title</td>
                <td>Description</td>
                <td>Images</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>
           <?php
           if($results<1){
            echo "<tr><td>Data tidak ditemukan</td></tr>";
           }

           else {

            foreach ($results as $data): ?>
            <tr>
              <td><a href="<?= base_url(); ?>news/detail/<?php echo $data->id_news; ?>"><?php echo $data->title; ?></a></td>
              <td><?php echo substr(htmlspecialchars($data->description), 0, 100); ?></td>
              <td><?php echo $data->images; ?></td> 
              <td>
                 <a class="button" href="<?= base_url(); ?>admin/news/edit/<?php echo $data->id_news; ?>">Edit</a> 
                | 
                <a onclick="return confirm('Apakah anda yakin ingin menghapus data <?php echo $data->title; ?>?');" class="button" href="<?= base_url(); ?>admin/news/delete/<?php echo $data->id_news; ?>">Delete</a> </td>     
            </tr>

          <?php endforeach;
          } ?>
   
          </tbody>
          </table>

          <?php  echo $links; ?>