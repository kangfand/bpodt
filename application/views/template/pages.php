<section id="mid-nav">
         <div class="container">
            <div class="row">
               <div class="col-sm-8">
                 
               </div>
               <div class="col-sm-4" style="">
                  
               </div>
            </div>
         </div>
      </section>
      <!-- start main -->
      <main>
         <div class="container">
            <div class="row">
             <div class="col-md-4">
                  <div class="sidebar">
                    
                     <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-profile-list" data-toggle="list" href="#profile" role="tab" aria-controls="home">Profil</a>
                        <a class="list-group-item list-group-item-action" id="list-sejarah-list" data-toggle="list" href="#sejarah" role="tab" aria-controls="profile">Sejarah</a>
                        <a class="list-group-item list-group-item-action" id="list-tugasdanfungsi-list" data-toggle="list" href="#tugasdanfungsi" role="tab" aria-controls="messages">Tugas dan Fungsi</a>
                        <a class="list-group-item list-group-item-action" id="list-strukturorganisasi-list" data-toggle="list" href="#strukturorganisasi" role="tab" aria-controls="settings">Struktur Organisasi</a>
                     </div>

                      <a data-toggle="modal" data-target=".bd-example-modal-lg"> <img src="../img/video.png" style="margin-top: 40px;" class="img-fluid"></a>
                     <!-- Berita Public Kominfo 
                        <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
                        <div id="gpr-kominfo-widget-container"></div>
                        
                        -->
                     <!--
                        <div class="card">
                           <div class="card-header">
                              Profile
                           </div>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item"><a href="">Sejarah</a></li>
                              <li class="list-group-item"><a href="">Tugas dan Fugsi</a></li>
                              <li class="list-group-item"><a href="">Struktur Organisasi</a></li>
                              <li class="list-group-item"><a href="">Unit Kerja</a></li>
                           </ul>
                        </div> -->
                  </div>
               </div>
               <div class="col-md-8">
                  <div class="content">
                     <div class="card">
                        <div class="card-body">
                           
                              <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane show active" id="profile" role="tabpanel" aria-labelledby="profile"> 
                                    <h1 class="h5">Dasar Hukum</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p>
                                 
                                 <h1 class="h5">Sifat Dan Kedudukan</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p>

                                 <h1 class="h5">Tujuan Pendirian</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p>

                                 <h1 class="h5">Tugas Otorita</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p>

                                    </div>
                                    <div class="tab-pane fade" id="sejarah" role="tabpanel" aria-labelledby="sejarah"><h1 class="h4">Sejarah</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p></div>
                                    <div class="tab-pane fade" id="tugasdanfungsi" role="tabpanel" aria-labelledby="list-messages-list"><h1 class="h4">Tugas dan Fungsi</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p></div>
                                    <div class="tab-pane fade" id="strukturorganisasi" role="tabpanel" aria-labelledby="strukturorganisasi"><h1 class="h4">Struktur Organisasi</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p></div>
                                 </div>

                        </div>
                     </div>
                  </div>
               </div>
              
            </div>
         </div>
      </main>
      <!-- end main -->
      <section id="sec-tautan">
         <div class="container">
            <div class="row nmb">
               <div class="col-md-12">
                  <div class="carousel slide text-center" id="myCarousel" data-ride="carousel">
                     <!--
                        <a class="left carousel-control mt-3 p-4" href="#myCarousel" data-slide="prev"> <i class="fa fa-chevron-left"></i> </a>
                          
                          <a class="right carousel-control mt-3 p-4" href="#myCarousel" data-slide="next"> <i class="fa fa-chevron-right"></i> </a>
                        
                          -->
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                           <div class="row">
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                           </div>
                        </div>
                        <div class="carousel-item ">
                           <div class="row">
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>