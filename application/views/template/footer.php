 <!-- start footer -->
      <footer>
         <div class="container">
            <div class="footer-widget">
               <div class="row">
                  <div class="col-md-4">
                     <div class="card bg-tema">
                        <!-- <h4 class="card-header">Alamat</h4> -->
                        <div class="card-body bg-tema">
                           <ul>
                              <li style="margin-bottom:10px; margin-top:10px;"><img src="<?= base_url('img/logo-white.png')?>"  class="img-fluid"/></li>
                              <li><i class="fa fa-map-marker icon"></i>
                                 <a href="https://www.google.co.id/maps/place/Badan+Pelaksana+Otorita+Danau+Toba/@3.576412,98.6644253,17z/data=!3m1!4b1!4m5!3m4!1s0x3031302c6af37423:0xe6025bb22f3329fd!8m2!3d3.576412!4d98.666614?hl=id" target="_blank">
                                 Jalan Kapten Patimura No.125, DARAT, Medan Baru, Kota Medan, Sumatera Utara 20153
                                 </a>
                              </li>
                              <li><i class="fa fa-phone icon"></i><a>(061) 42002060</a></li>
                              <li><i class="fa fa-envelope icon"></i>
                                 <a href="mailto:humas@mail.kominfo.go.id">humas@mail.bpodt.go.id </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="card" >
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.0481251214883!2d98.66442531422862!3d3.5764119973973743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031302c6af37423%3A0xe6025bb22f3329fd!2sBadan+Pelaksana+Otorita+Danau+Toba!5e0!3m2!1sid!2sid!4v1504018136839" width="100%" height="230" scrolling="no" marginheight="0"  frameborder="0" style="border:0" allowfullscreen></iframe>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="card">
                        <!--
                           <ul class="list-group list-group-flush ">
                              <li class="list-group-item active text-gray-dark">Tautan <strong>Link</strong></li>
                              <li class="list-group-item text-muted">Tentang Kami</li>
                              <li class="list-group-item text-muted">Peta Situs</li>
                              <li class="list-group-item text-muted">Prasyarat</li>
                              <li class="list-group-item text-muted">Hubungi Kami</li>
                           </ul>
                           -->
                        <ul class="list-group list-group-flush ">
                           <li class="list-group-item active text-gray-dark">Statistic <strong>Visitor</strong></li>
                           <li class="list-group-item text-muted">Pengunjung Hari Ini : <strong>135</strong></li>
                           <li class="list-group-item text-muted">Pengunjung Minggu Ini : <strong>543</strong></li>
                           <li class="list-group-item text-muted">Pengunjung Bulan ini : <strong>23479</strong></li>
                           <li class="list-group-item text-muted">Sedang Online : <strong>8</strong> </li>
                        </ul>
                        <!--
                           <h4 class="card-header">Statistic</h4>
                           <div class="card-body">
                              <p class="card-text">
                                 Pengunjung Hari Ini : 10 <br>
                                 Pengunjung Minggu Ini : 1000 <br>
                                 Pengunjung Bulan Ini :10000 <br>
                              </p>
                           </div>
                           -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer-bar">
            <div class="container">
               <div class="row nmb">
                  <div class="col-md-6">
                     <div class="footer-link">
                        <ul class="nav">
                           <li class="nav-item"><a class="nav-link" href="<?= base_url('profile') ?>">Profil</a></li>
                           <li class="nav-item"><a class="nav-link" href="<?= base_url('tautan') ?>">Tautan</a></li>
                           <li class="nav-item"><a class="nav-link" href="<?= base_url('FAQ') ?>">FAQ</a></li>
                           <li class="nav-item"><a class="nav-link" href="<?= base_url('petasitus') ?>">Peta Situs</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="copyright text-muted text-right">
                        Hak Cipta © 2017 Badan Pengelola Otorita Danau Toba (BPODT)
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript External-->
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
      <script type="text/javascript"></script>
   </body>
</html>