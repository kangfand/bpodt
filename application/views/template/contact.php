<div class="container">
   <div class="row">
      <div class="col-sm-6">
         
         <h1 class="h4" style="padding-top: 30px;">Have a question? Get in touch with us!</h1>
                                 <form action="" method="">
                                    <fieldset class="form-group">
                                       <label for="formName">Your Name:</label>
                                       <input id="formName" class="form-control" type="text" placeholder="Your Name" required>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label for="formEmail1">Email address:</label>
                                       <input id="formEmail1" class="form-control" type="email" placeholder="Enter email" required>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label for="formMessage">Your Message:</label>
                                       <textarea id="formMessage" class="form-control" rows="3" placeholder="Your message" required></textarea>
                                    </fieldset>
                                    <fieldset class="form-group text-left">
                                       <button class="btn btn-primary" type="submit">Send Message</button>
                                    </fieldset>
                                 </form>   

      </div>
   </div>

</div>
