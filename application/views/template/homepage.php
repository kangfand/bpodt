<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Badan Pelaksana Otorita Danau Toba (BPODT)</title>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
      <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style type="text/css">

      /*
      ===================================================
      CSS Decorated By    : Idmetafora (idmetafora.com)
      Author              : IND8            
      ===================================================
      */
         .row {
         margin-bottom: 20px;
         }
         .navigasi {
         background-color: #253C80;
         }
         header{
         background-color: #2D458D;
         border-top: 8px solid #4FA0F1;
         }
         .header{
         height: 150px;
         }
         .logo{
         margin-top:15px;
         }
         /* Navigasi Anim */
         .dropdown-menu {
         -webkit-transform: scale(1, 0);
         transform: scale(1, 0);
         -webkit-transition: all 0.3s ease;
         transition: all 0.3s ease;
         opacity: 0;
         display: block;
         -webkit-transform-origin: top;
         transform-origin: top;
         -webkit-animation-fill-mode: forwards;
         animation-fill-mode: forwards;
         }
         .show > .dropdown-menu {
         -webkit-transform: scale(1, 1);
         transform: scale(1, 1);
         opacity: 1;
         }
         
        
         .navbar-expand-lg .navbar-nav .nav-link {
         padding-left: 20px;
         padding-right: 20px;
         }
         .actived{
         background-color: #1E2F65;
         border-radius: 0px;
         transition: 0.5s all ease;
         -webkit-transition: 0.5s all ease;
         }
         .top-nav a {
         font-size: 1rem;
         color: #1E2F65;
         text-decoration: none;
         border-bottom: 3px solid transparent;
         transition: .25s;
         text-transform: uppercase;
         }
         .top-nav a:hover {
         color: #fff;
         /* border-color: #1E2F65; */
         background-color: #223673;
         /*border-radius: 5px;*/
         }
         .dropdown-menu{
         top:100%;
         border-radius: 0px;
         }

         .dropdown:hover .dropdown-menu {
         display: block;
         margin-top: 0; 
         -webkit-transform: scale(1, 1);
         transform: scale(1, 1);
         opacity: 1;
         }

          .dropdown-toggle::after{
         display: none;
         }

         .navbar{
         padding-left: 0px;
         padding-bottom: 0px;
         padding-top: 0px;
         }

         /* Large devices (desktops, 992px and up) */
         @media (min-width: 992px) and (max-width: 1199px) { 
         

         
         }


         /* Extra small devices (portrait phones, less than 576px) */
         @media (max-width: 575px) { 
         #sec-list{
         margin-top: 35px;
         }
         }
         .navbar-dark .navbar-toggler {
         margin-bottom: 10px;
         margin-top: 10px;
         }
         /* section */
         .list{
         padding: 30px;
         }
         #futured{
         background-color: #DDDBDC;
         padding-bottom: 40px;
         padding-top: 40px;
         }
         #sec-program{
         background-color: #f0f0f0;
         padding-bottom: 40px;
         padding-top: 40px;
         }
         #sec-tautan{
         background-color: #f0f0f0;
         padding-top: 20px;
         }
         #mid-nav{
         background-color: #E8E6E7;
         padding-top: 20px;
         }

         /* Section Slider */
         /* Carousel
         .carousel-indicators {
         bottom: -30px;
         }
         .carousel-indicators li {
         background: rgba(33, 33, 33, 0.1);
         }
         .carousel-indicators li:focus, .carousel-indicators li:hover {
         background: rgba(33, 33, 33, 0.5);
         }
         .carousel-indicators .active {
         background: rgba(33, 33, 33, 0.75);
         }
         .carousel-control-next,
         .carousel-control-prev {
         color: #212121;
         }
         .carousel-control-next:focus, .carousel-control-next:hover,
         .carousel-control-prev:focus,
         .carousel-control-prev:hover {
         color: #111;
         }
         .footer {
         padding-top: 24px;
         padding-bottom: 24px;
         background: #212121;
         }
         .footer li:nth-of-type(n+2) {
         margin-left: 8px;
         }
         .footer a {
         font-size: 18px;
         color: rgba(255, 255, 255, 0.5);
         -webkit-transition: color .235s ease-in-out;
         transition: color .235s ease-in-out;
         }
         .footer a:focus, .footer a:hover {
         color: rgba(255, 255, 255, 0.25);
         }
         */
        
         /* Main Content */
         main{
         margin-top: 40px;
         margin-bottom: 40px;
         }
         .title{
         text-transform: uppercase;
         }
         .tab-active{
         border-top: 10px solid #444 ;
         }
         footer{
         padding: 40px 0px 0px 0px;
         background-color: #1E2F65;
         }
         .footer-bar{
         background-color: #14224F;
         padding-top: 8px;
         padding-bottom: 8px;
         }
         .footer-widget{
         padding-bottom: 20px;
         }
         .copyright{
         padding-top: 10px;
         padding-right: 20px;
         font-size: 12px;
         }
         footer ul{
         list-style: none;
         padding: 0px;
         }
         footer ul li{
         padding-bottom: 3px;
         color: #fff;
         font-size: 14px;
         }
         footer ul li a{
         color: #fff;
         }
         footer ul li a:hover, footer ul li a.active{
         color: #E8E6E7;
         text-decoration: none;
         }
         footer .icon{
         margin-right:10px; color:#fff;
         }
         /* Sosial */
         .sosial li:nth-of-type(n+2) {
         margin-left: 8px;
         }
         .sosial a {
         font-size: 18px;
         color: rgba(255, 255, 255, 0.5);
         -webkit-transition: color .235s ease-in-out;
         transition: color .235s ease-in-out;
         }
         .sosial a:focus, .footer a:hover {
         color: rgba(255, 255, 255, 0.25);
         background-color: none !important;
         }
         .sosial li a:hover{
         background-color: #253c80;
         }
         /* Utilities */
         .nmb{
         margin-bottom: 0px;
         }
         .bg-tema{
         background-color: #1E2F65;
         border: none;
         }
      </style>
   </head>
   <body>

      <!-- Modal 
         <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
             <div class="modal-content">
               <div class="modal-header">
                 <h5 class="modal-title" id="exampleModalLabel">Video</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
                 </button>
               </div>
               <div class="modal-body">
               
               </div>
               <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               </div>
             </div>
           </div>
         </div> -->

      <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/SJw3EhK_zEQ?rel=0&amp;autoplay=0&showinfo=1&controls=0" frameborder="0" allowfullscreen autoplay></iframe>
               </div>
            </div>
         </div>
      </div>
      <!-- start header -->
      <header>
         <!--
            <div class="container">
                     
                     <div class="row">
                              <div class="col-md-12">
                                       <div class="card">
                                                <div class="card-body">
                                                         Header
                                                </div>
                                       </div>
                              </div>
                     </div>
            </div>
            -->
         <div class="header"> 
         <div class="container">
         <img src="../img/logo.png" class="logo img-fluid">
         </div>
         
         </div>
         <div class="container ">
            <div class="row nmb">
               <div class="col-sm-12">
                  <nav class="navbar navbar-expand-lg navbar-dark top-nav" style="background-color: #253C80;">
                     <!--<a class="navbar-brand visible-md" href="#">Navbar</a> -->
                     <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     MENU
                     </button>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                           <li class="nav-item active actived">
                              <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="pages.html" role="button" aria-haspopup="true" aria-expanded="false">Profile</a>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="pages.html#list-profile">Sejarah</a>
                                 <a class="dropdown-item" href="#">Tugas dan Fugsi</a>
                                 <a class="dropdown-item" href="#">Struktur Organisasi</a>
                                 <a class="dropdown-item" href="#">Unit Kerja</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Informasi Publik</a>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="#">Siaran Pers</a>
                                 <a class="dropdown-item" href="#">Laporan Data</a>
                                 <a class="dropdown-item" href="#">Agenda</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kegiatan</a>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="#">Berita Foto</a>
                              </div>
                           </li>
                           <li class="nav-item"><a class="nav-link" href="">Media Galeri</a></li>
                           <li class="nav-item"><a class="nav-link" href="">Hubungi Kami</a></li>
                        </ul>
                        <!--
                           -->
                        <div class="sosial text-right">
                           <ul class="list-inline mb-0 text-center">
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-facebook-official"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-twitter"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-instagram"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-youtube"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-google-plus"></span></a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </header>
      <section id="futured">
         <div class="futured container">
            <div class="row nmb">
               <div class="col-sm-8">
                  <section id="sec-slider" class="sec-slider">
                     <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                           <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                           <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                           <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                           <div class="carousel-item active">
                              <img class="d-block w-100" src="../img/slider1.jpg" alt="First slide">
                              <div class="carousel-caption d-none d-md-block text-left">
                                 <small>Berita Terbaru&nbsp; &nbsp;27-08-2017</small>
                                 <h4>Capaian Kinerja Kemenpar RI Tahun 2015-2017</h4>
                              </div>
                           </div>
                           <div class="carousel-item">
                              <img class="d-block w-100" src="../img/slider2.jpg" alt="Second slide">
                              <div class="carousel-caption d-none d-md-block">
                                 <h3>Pesona Danau Toba</h3>
                                 <p>Penyebrangan ke Tomo Samosir</p>
                              </div>
                           </div>
                           <div class="carousel-item">
                              <img class="d-block w-100" src="../img/slider3.jpg" alt="Third slide">
                              <div class="carousel-caption d-none d-md-block">
                                 <h3>Pelabuhan Penyebrangan Tomo</h3>
                                 <p>Kondisi suasana pelabuhan dabau toba</p>
                              </div>
                           </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                     </div>
                  </section>
               </div>
               <div class="col-sm-4">
                  <section id="sec-list" class="sec-list">
                     <div class="card" >
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item active" style="padding:18px; background-color:#253C80 ; border-bottom:8px solid #4FA0F1; font-size:18px;">Siaran <strong>Pers</strong></li>
                           <li class="list-group-item list-group-item-action">Dapibus ac facilisis in</li>
                           <li class="list-group-item list-group-item-action">Vestibulum at eros</li>
                           <li class="list-group-item list-group-item-action">Cras justo odio</li>
                           <li class="list-group-item list-group-item-action">Dapibus ac facilisis in</li>
                           <li class="list-group-item list-group-item-action">Vestibulum at eros</li>
                           <li class="list-group-item list-group-item-action">Cras justo odio</li>
                        </ul>
                     </div>
                     <!--
                        <div class="card text-center list">
                           <div class="card-block">
                              <h4 class="card-title h5">Individual</h4>
                              <h5 class="card-title h6">100 &euro;/month</h5>
                              <p class="card-text">The flexible place to grow your projects in a community of start-ups, freelancers and entrepreneurs, flex desk.</p>
                              <a href="#" class="btn btn-primary">Find out more</a>
                           </div>
                           
                        </div>
                        -->
                  </section>
               </div>
            </div>
         </div>
      </section>
      <section id="sec-program" class="sec-program">
         <div class="container">
            <h1 class="h4 mb-5 text-center ">Program Prioritas<br>
               <small style="font-size: 14px;">Sesuai Rencana Strategis Kementerian Komunikasi dan Informatika <br>Tahun 2015 - 2019</small>
            </h1>
            <div class="row justify-content-center">
               <div class="col-md-4">
                  <div class="card text-center">
                     <div class="card-body">
                        <img src="https://www.kominfo.go.id/images/4.png">
                        <!--<h4 class="card-title h5">Individual</h4>-->
                        <h5 class="card-title h6" style="margin-top: 20px;">Judul Ketiga</h5>
                        <p class="card-text">The flexible place to grow your projects in a community of start-ups, freelancers and entrepreneurs, flex desk.</p>
                        <a href="#" class="btn btn-primary">Selengkapnya</a>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="card text-center">
                     <div class="card-body">
                        <img src="https://www.kominfo.go.id/images/1.png">
                        <!--<h4 class="card-title h5">Individual</h4>-->
                        <h5 class="card-title h6" style="margin-top: 20px;">Judul Kedua</h5>
                        <p class="card-text">Your own personal area in an open space: desk, drawers, locker and bookcase to accessorise however you like.</p>
                        <a href="#" class="btn btn-primary">Selengkapnya</a>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="card text-center">
                     <div class="card-body justify-content-center">
                        <img src="https://www.kominfo.go.id/images/3.png">
                        <!--<h4 class="card-title h5">Individual</h4>-->
                        <h5 class="card-title h6" style="margin-top: 20px;">Judul Ketiga</h5>
                        <p class="card-text">Your own private space, perfect for work teams of up to 50 people, inside a structure that’s always in motion</p>
                        <a href="#" class="btn btn-primary">Selengkapnya</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="mid-nav">
         <div class="container">
            <div class="row">
               <div class="col-sm-8">
                  <ul class="nav nav-tabs" role="tablist">
                     <li class="nav-item">
                        <a class="nav-link active tab-active" href="#profile" role="tab" data-toggle="tab">
                           <h5 class="title">Terbaru</h5>
                           <small>Rangkuman Berita</small>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">
                           <h5 class="title">Pengumuman</h5>
                           <small>Pengumuman terbaru</small>
                        </a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="#references" role="tab" data-toggle="tab">
                           <h5 class="title">Unduhan</h5>
                           <small>Laporan terbaru</small>
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="col-sm-4" style="margin-top: 6px;">
                  <form class="form-inline my-2 my-lg-0" style="margin-top: -10px;">
                     <input class="form-control mr-sm-2" size="25px" type="text" placeholder="Temukan Sesuatu ..." aria-label="Search">
                     <button class="btn btn-primary my-2 my-sm-0" style="width: 100px;" type="submit">Search</button>
                  </form>
               </div>
            </div>
         </div>
      </section>
      <!-- start main -->
      <main>
         <div class="container">
            <div class="row">
               <div class="col-md-8">
                  <div class="content">
                     <div class="card">
                        <div class="card-body">
                           <div class="tab-content">
                              <div role="tabpanel" class="tab-pane fade show active " id="profile">
                                 <h1 class="h4">About us</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p>
                                 <div class="media">
                                    <img class="d-flex mr-3" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d7f4f96%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d7f4f96%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Generic placeholder image">
                                    <div class="media-body">
                                       <h5 class="mt-0">Media heading</h5>
                                       Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                                    </div>
                                 </div>
                                 <div class="media" style="margin-top: 15px;">
                                    <img class="d-flex mr-3" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d7f4f96%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d7f4f96%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Generic placeholder image">
                                    <div class="media-body">
                                       <h5 class="mt-0">Media heading</h5>
                                       Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                                    </div>
                                 </div>
                                 <div class="media" style="margin-top: 15px;">
                                    <img class="d-flex mr-3" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d7f4f96%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d7f4f96%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Generic placeholder image">
                                    <div class="media-body">
                                       <h5 class="mt-0">Media heading</h5>
                                       Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                                    </div>
                                 </div>
                                 <div class="media" style="margin-top: 15px;">
                                    <img class="d-flex mr-3" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d7f4f96%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d7f4f96%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Generic placeholder image">
                                    <div class="media-body">
                                       <h5 class="mt-0">Media heading</h5>
                                       Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                                    </div>
                                 </div>
                                 <br>
                                 <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade" id="list-home" role="tabpanel" aria-labelledby="list-home-list"> 
                                    <h1 class="h4">Profil</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p>

                                    </div>
                                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list"><h1 class="h4">Sejarah</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p></div>
                                    <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list"><h1 class="h4">Tugas dan Fungsi</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p></div>
                                    <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list"><h1 class="h4">Struktur Organisasi</h1>
                                 <p class="mt-4 mb-4">Co-working spaces are brilliant for smaller companies of up to 4 people who want a regular workspace. Cost effective, flexible and full of a vibrant energy that comes from hundreds of like-minded people going it alone.</p></div>
                                 </div>
                              </div>
                              <!-- Pengumuman -->
                              <div role="tabpanel" class="tab-pane fade" id="buzz">
                                 <div class="card-deck">
                                    <div class="card">
                                       <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22239%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20239%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d86fc08%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d86fc08%22%3E%3Crect%20width%3D%22239%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2288.7578125%22%20y%3D%2295.1%22%3E239x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                                       <div class="card-body">
                                          <h4 class="card-title">Bekraf game Prime</h4>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted">28-07-2017</small>
                                       </div>
                                    </div>
                                    <div class="card">
                                       <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22239%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20239%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d86fc08%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d86fc08%22%3E%3Crect%20width%3D%22239%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2288.7578125%22%20y%3D%2295.1%22%3E239x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                                       <div class="card-body">
                                          <h4 class="card-title">Bekraf game Prime</h4>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted">30-07-2017</small>
                                       </div>
                                    </div>
                                    <div class="card">
                                       <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22239%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20239%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d86fc08%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d86fc08%22%3E%3Crect%20width%3D%22239%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2288.7578125%22%20y%3D%2295.1%22%3E239x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                                       <div class="card-body">
                                          <h4 class="card-title">Bekraf game Prime</h4>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted">02-08-2017</small>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="card-deck" style="margin-top: 20px;">
                                    <div class="card">
                                       <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22239%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20239%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d86fc08%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d86fc08%22%3E%3Crect%20width%3D%22239%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2288.7578125%22%20y%3D%2295.1%22%3E239x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                                       <div class="card-body">
                                          <h4 class="card-title">Bekraf game Prime</h4>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted">28-07-2017</small>
                                       </div>
                                    </div>
                                    <div class="card">
                                       <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22239%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20239%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d86fc08%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d86fc08%22%3E%3Crect%20width%3D%22239%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2288.7578125%22%20y%3D%2295.1%22%3E239x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                                       <div class="card-body">
                                          <h4 class="card-title">Bekraf game Prime</h4>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted">28-07-2017</small>
                                       </div>
                                    </div>
                                    <div class="card">
                                       <img class="card-img-top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22239%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20239%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_15e2d86fc08%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_15e2d86fc08%22%3E%3Crect%20width%3D%22239%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2288.7578125%22%20y%3D%2295.1%22%3E239x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
                                       <div class="card-body">
                                          <h4 class="card-title">Bekraf game Prime</h4>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted">28-07-2017</small>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div role="tabpanel" class="tab-pane fade" id="references">
                                 <h1 class="h4">Have a question? Get in touch with us!</h1>
                                 <form action="" method="">
                                    <fieldset class="form-group">
                                       <label for="formName">Your Name:</label>
                                       <input id="formName" class="form-control" type="text" placeholder="Your Name" required>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label for="formEmail1">Email address:</label>
                                       <input id="formEmail1" class="form-control" type="email" placeholder="Enter email" required>
                                    </fieldset>
                                    <fieldset class="form-group">
                                       <label for="formMessage">Your Message:</label>
                                       <textarea id="formMessage" class="form-control" rows="3" placeholder="Your message" required></textarea>
                                    </fieldset>
                                    <fieldset class="form-group text-left">
                                       <button class="btn btn-primary" type="submit">Send Message</button>
                                    </fieldset>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="sidebar">
                     <a data-toggle="modal" data-target=".bd-example-modal-lg"> <img src="../img/video.png" style="margin-bottom: 40px;" class="img-fluid"></a>
                     <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Profil</a>
                        <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Sejarah</a>
                        <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Tugas dan Fungsi</a>
                        <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Struktur Organisasi</a>
                     </div>
                     <!-- Berita Public Kominfo 
                        <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
                        <div id="gpr-kominfo-widget-container"></div>
                        
                        -->
                     <!--
                        <div class="card">
                           <div class="card-header">
                              Profile
                           </div>
                           <ul class="list-group list-group-flush">
                              <li class="list-group-item"><a href="">Sejarah</a></li>
                              <li class="list-group-item"><a href="">Tugas dan Fugsi</a></li>
                              <li class="list-group-item"><a href="">Struktur Organisasi</a></li>
                              <li class="list-group-item"><a href="">Unit Kerja</a></li>
                           </ul>
                        </div> -->
                  </div>
               </div>
            </div>
         </div>
      </main>
      <!-- end main -->
      <section id="sec-tautan">
         <div class="container">
            <div class="row nmb">
               <div class="col-md-12">
                  <div class="carousel slide text-center" id="myCarousel" data-ride="carousel">
                     <!--
                        <a class="left carousel-control mt-3 p-4" href="#myCarousel" data-slide="prev"> <i class="fa fa-chevron-left"></i> </a>
                          
                          <a class="right carousel-control mt-3 p-4" href="#myCarousel" data-slide="next"> <i class="fa fa-chevron-right"></i> </a>
                        
                          -->
                     <div class="carousel-inner">
                        <div class="carousel-item active">
                           <div class="row">
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                           </div>
                        </div>
                        <div class="carousel-item ">
                           <div class="row">
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                              <div class="col-md-3"><a href="#"><img src="../img/tautan.png" class="img-fluid"></a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- start footer -->
      <footer>
         <div class="container">
            <div class="footer-widget">
               <div class="row">
                  <div class="col-md-4">
                     <div class="card bg-tema">
                        <!-- <h4 class="card-header">Alamat</h4> -->
                        <div class="card-body bg-tema">
                           <ul>
                              <li style="margin-bottom:10px; margin-top:10px;"><img src="../img/logo-white.png"  class="img-fluid"/></li>
                              <li><i class="fa fa-map-marker icon"></i>
                                 <a href="https://www.google.co.id/maps/place/Badan+Pelaksana+Otorita+Danau+Toba/@3.576412,98.6644253,17z/data=!3m1!4b1!4m5!3m4!1s0x3031302c6af37423:0xe6025bb22f3329fd!8m2!3d3.576412!4d98.666614?hl=id" target="_blank">
                                 Jalan Kapten Patimura No.125, DARAT, Medan Baru, Kota Medan, Sumatera Utara 20153
                                 </a>
                              </li>
                              <li><i class="fa fa-phone icon"></i><a>(061) 42002060</a></li>
                              <li><i class="fa fa-envelope icon"></i>
                                 <a href="mailto:humas@mail.kominfo.go.id">humas@mail.bpodt.go.id </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="card" >
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.0481251214883!2d98.66442531422862!3d3.5764119973973743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3031302c6af37423%3A0xe6025bb22f3329fd!2sBadan+Pelaksana+Otorita+Danau+Toba!5e0!3m2!1sid!2sid!4v1504018136839" width="100%" height="230" scrolling="no" marginheight="0"  frameborder="0" style="border:0" allowfullscreen></iframe>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="card">
                        <!--
                           <ul class="list-group list-group-flush ">
                              <li class="list-group-item active text-gray-dark">Tautan <strong>Link</strong></li>
                              <li class="list-group-item text-muted">Tentang Kami</li>
                              <li class="list-group-item text-muted">Peta Situs</li>
                              <li class="list-group-item text-muted">Prasyarat</li>
                              <li class="list-group-item text-muted">Hubungi Kami</li>
                           </ul>
                           -->
                        <ul class="list-group list-group-flush ">
                           <li class="list-group-item active text-gray-dark">Statistic <strong>Visitor</strong></li>
                           <li class="list-group-item text-muted">Pengunjung Hari Ini : <strong>135</strong></li>
                           <li class="list-group-item text-muted">Pengunjung Minggu Ini : <strong>543</strong></li>
                           <li class="list-group-item text-muted">Pengunjung Bulan ini : <strong>23479</strong></li>
                           <li class="list-group-item text-muted">Sedang Online : <strong>8</strong> </li>
                        </ul>
                        <!--
                           <h4 class="card-header">Statistic</h4>
                           <div class="card-body">
                              <p class="card-text">
                                 Pengunjung Hari Ini : 10 <br>
                                 Pengunjung Minggu Ini : 1000 <br>
                                 Pengunjung Bulan Ini :10000 <br>
                              </p>
                           </div>
                           -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="footer-bar">
            <div class="container">
               <div class="row nmb">
                  <div class="col-md-6">
                     <div class="footer-link">
                        <ul class="nav">
                           <li class="nav-item"><a class="nav-link" href="">Profil</a></li>
                           <li class="nav-item"><a class="nav-link" href="">Tautan</a></li>
                           <li class="nav-item"><a class="nav-link" href="">FAQ</a></li>
                           <li class="nav-item"><a class="nav-link" href="">Peta Situs</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="copyright text-muted text-right">
                        Hak Cipta © 2017 Badan Pengelola Otorita Danau Toba (BPODT)
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- end footer -->
      <!-- Javascript External-->
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
      <script type="text/javascript"></script>
   </body>
</html>