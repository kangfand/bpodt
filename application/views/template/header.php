<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Badan Pelaksana Otorita Danau Toba (BPODT)</title>
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
      <link rel="stylesheet" href=" https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <style type="text/css">

      /*
      ===================================================
      CSS Decorated By    : Idmetafora (idmetafora.com)
      Author              : IND8            
      ===================================================
      */
         .row {
         margin-bottom: 20px;
         }
         .navigasi {
         background-color: #253C80;
         }
         header{
         background-color: #2D458D;
         border-top: 8px solid #0093c2;
         }
         .header{
         height: 150px;
         }
         .logo{
         margin-top: 20px;
         width: 350px;
         margin-left: -84px;
         }
         /* Navigasi Anim */
         .dropdown-menu {
         -webkit-transform: scale(1, 0);
         transform: scale(1, 0);
         -webkit-transition: all 0.3s ease;
         transition: all 0.3s ease;
         opacity: 0;
         display: block;
         -webkit-transform-origin: top;
         transform-origin: top;
         -webkit-animation-fill-mode: forwards;
         animation-fill-mode: forwards;
         }
         .show > .dropdown-menu {
         -webkit-transform: scale(1, 1);
         transform: scale(1, 1);
         opacity: 1;
         }
         
        
         .navbar-expand-lg .navbar-nav .nav-link {
         padding-left: 20px;
         padding-right: 20px;
         }
         .actived{
         background-color: #1E2F65;
         border-radius: 0px;
         transition: 0.5s all ease;
         -webkit-transition: 0.5s all ease;
         }
         .top-nav a {
         font-size: 1rem;
         color: #1E2F65;
         text-decoration: none;
         border-bottom: 3px solid transparent;
         transition: .25s;
         text-transform: uppercase;
         }
         .top-nav a:hover {
         color: #fff;
         /* border-color: #1E2F65; */
         background-color: #223673;
         /*border-radius: 5px;*/
         }
         .dropdown-menu{
         top:100%;
         border-radius: 0px;
         }

         .dropdown:hover .dropdown-menu {
         display: block;
         margin-top: 0; 
         -webkit-transform: scale(1, 1);
         transform: scale(1, 1);
         opacity: 1;
         }

          .dropdown-toggle::after{
         display: none;
         }

         .navbar{
         padding-left: 0px;
         padding-bottom: 0px;
         padding-top: 0px;
         }

         /* Large devices (desktops, 992px and up) */
         @media (min-width: 992px) and (max-width: 1199px) { 
         

         
         }


         /* Extra small devices (portrait phones, less than 576px) */
         @media (max-width: 575px) { 
         #sec-list{
         margin-top: 35px;
         }
         }
         .navbar-dark .navbar-toggler {
         margin-bottom: 10px;
         margin-top: 10px;
         }
         /* section */
         .list{
         padding: 30px;
         }
         #futured{
         background-color: #DDDBDC;
         padding-bottom: 40px;
         padding-top: 40px;
         }
         #sec-program{
         background-color: #f0f0f0;
         padding-bottom: 40px;
         padding-top: 40px;
         }
         #sec-tautan{
         background-color: #f0f0f0;
         padding-top: 20px;
         }
         #mid-nav{
         background-color: #E8E6E7;
         /*padding-top: 20px; */
         }

         /* Section Slider */
         /* Carousel
         .carousel-indicators {
         bottom: -30px;
         }
         .carousel-indicators li {
         background: rgba(33, 33, 33, 0.1);
         }
         .carousel-indicators li:focus, .carousel-indicators li:hover {
         background: rgba(33, 33, 33, 0.5);
         }
         .carousel-indicators .active {
         background: rgba(33, 33, 33, 0.75);
         }
         .carousel-control-next,
         .carousel-control-prev {
         color: #212121;
         }
         .carousel-control-next:focus, .carousel-control-next:hover,
         .carousel-control-prev:focus,
         .carousel-control-prev:hover {
         color: #111;
         }
         .footer {
         padding-top: 24px;
         padding-bottom: 24px;
         background: #212121;
         }
         .footer li:nth-of-type(n+2) {
         margin-left: 8px;
         }
         .footer a {
         font-size: 18px;
         color: rgba(255, 255, 255, 0.5);
         -webkit-transition: color .235s ease-in-out;
         transition: color .235s ease-in-out;
         }
         .footer a:focus, .footer a:hover {
         color: rgba(255, 255, 255, 0.25);
         }
         */
        
         /* Main Content */
         main{
         margin-top: 40px;
         margin-bottom: 40px;
         }
         .title{
         text-transform: uppercase;
         margin: 0px;
         font-size: 16px;
         }
         .tab-active{
         border-top: 10px solid #444 ;
         }
         footer{
         padding: 40px 0px 0px 0px;
         background-color: #1E2F65;
         }
         .footer-bar{
         background-color: #14224F;
         padding-top: 8px;
         padding-bottom: 8px;
         }
         .copyright{
         padding-top: 10px;
         padding-right: 20px;
         font-size: 12px;
         }
         footer ul{
         list-style: none;
         padding: 0px;
         }
         footer ul li{
         padding-bottom: 3px;
         color: #fff;
         font-size: 14px;
         }
         footer ul li a{
         color: #fff;
         }
         footer ul li a:hover, footer ul li a.active{
         color: #E8E6E7;
         text-decoration: none;
         }
         footer .icon{
         margin-right:10px; color:#fff;
         }
         /* Sosial */
         .sosial li:nth-of-type(n+2) {
         margin-left: 8px;
         }
         .sosial a {
         font-size: 18px;
         color: rgba(255, 255, 255, 0.5);
         -webkit-transition: color .235s ease-in-out;
         transition: color .235s ease-in-out;
         }
         .sosial a:focus, .footer a:hover {
         color: rgba(255, 255, 255, 0.25);
         background-color: none !important;
         }
         .sosial li a:hover{
         background-color: #253c80;
         }
         /* Utilities */
         .nmb{
         margin-bottom: 0px;
         }
         .bg-tema{
         background-color: #1E2F65;
         border: none;
         }
         .box {
             background-color: #fff;
             border: 1px solid #eeeeee;
             /*border-bottom: 0; */
         }
         .p0 {
             padding: 0 !important;
         }
         .m0 {
            margin: 0 !important;
         }
         .rm{
            padding-bottom: 20px; padding-right:20px;
         }
         .p30{
            padding-left: 30px;
         }

         .news-content{min-height:106px;}
.news-content>img{width:100%; height: 153px;}
.news-content>span{background-color:#0b56a4; color:#fff; padding:5px; float:right; font-size:8pt; font-weight:bold; margin-right:25px; margin-top:-12px; position:absolute; right:0;}
.news-content>h5{font-size:11pt; font-weight:bold; color:#0b56a4; margin:9px 0 0 0; /*padding:12px 30px;*/  max-height: 30px; overflow: hidden; padding: 12px 10px 0px 30px;}
.news-content>p{font-size:11pt; /*padding:0 30px;*/ /*line-height:20px;*/ margin-bottom:12px; min-height: 30px; max-height: 51px; overflow: hidden; padding: 10px 10px 0px 30px;}
/*.news-content>p::after {content: "...";}*/


       
 .news-content>span.date{position:relative; background:transparent; float:left; color:#0b56a4; padding-left:30px;}

 .caption {
    position: absolute;
    width: 91%;
    margin-top: -283px;
}

@media (max-width: 991px) { 
.news-content>h5{padding: 12px 15px;}
   .news-content>p{padding: 0 15px;max-height: 46px;}
   .news-content>span{/*display:none;*/}
   .more-read{margin-right:30px; width:30px !important; height:auto !important; min-height: inherit !important;}
}

.nav-item a{
   color:#c1c1c1;
}
.nav-tabs .nav-link{
   border: #ddd solid 1px;
}

a:hover{
   text-decoration: none;  
   font-weight: 500;
}

/*
.logo {
    margin-top: 20px;
    width: 300px;
 }
.badge{
    width: 200px;
}
*/
      </style>
   </head>
   <body>

        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/SJw3EhK_zEQ?rel=0&amp;autoplay=0&showinfo=1&controls=0" frameborder="0" allowfullscreen autoplay></iframe>
               </div>
            </div>
         </div>
      </div>
      
            <!-- start header -->
    <img src="<?= base_url('img/badge.png')?>" style="top: 0px;
    right: 0px;
    position: absolute;">
      <header>
           
         <div class="header"> 
         <div class="container">
         <img src="<?= base_url('img/logo.png')?>" class="logo img-fluid">
         </div>
         
         </div>
         <div class="container ">
            <div class="row nmb">
               <div class="col-sm-12">
                  <nav class="navbar navbar-expand-lg navbar-dark top-nav" style="background-color: #253C80;">
                    
                     <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                     MENU
                     </button>
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                          
                           <li class="nav-item <?php if (current_url() == base_url()){ echo 'actived';}?> ">
                              <a class="nav-link" href="<?= base_url()?>">Home <span class="sr-only">(current)</span></a>
                           </li>

                           <li class="nav-item dropdown <?php if (current_url() == base_url('/profile')){ echo 'actived' ;}?> ">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="<?= base_url('profile')?>" role="button" aria-haspopup="true" aria-expanded="false"  href="<?= base_url('profile')?>">Profile</a>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="<?= base_url('profile')?>">Sejarah</a>
                                 <a class="dropdown-item" href="<?= base_url('profile')?>">Tugas dan Fugsi</a>
                                 <a class="dropdown-item" href="<?= base_url('profile')?>">Struktur Organisasi</a>
                                 <a class="dropdown-item" href="<?= base_url('profile')?>">Unit Kerja</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Informasi Publik</a>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="<?= base_url('infopublik/siaranpers')?>">Siaran Pers</a>
                                 <a class="dropdown-item" href="<?= base_url('infopublik/laporan')?>">Laporan Data</a>
                                 <a class="dropdown-item" href="<?= base_url('infopublik/agenda')?>">Agenda</a>
                              </div>
                           </li>
                           <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Kegiatan</a>
                              <div class="dropdown-menu">
                                 <a class="dropdown-item" href="<?= base_url('kegiatan/beritafoto')?>">Berita Foto</a>
                              </div>
                           </li>
                           <li class="nav-item"><a class="nav-link" href="<?= base_url('mediagaleri')?>" >Media Galeri</a></li>
                           <li class="nav-item"><a class="nav-link" href="<?= base_url('kontak')?>" >Hubungi Kami</a></li>
                        </ul>
                        <!--
                           -->
                        <div class="sosial text-right">
                           
                           
                           <ul class="list-inline mb-0 text-center">
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-facebook-official"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-twitter"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-instagram"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-youtube"></span></a>
                              </li>
                              <li class="list-inline-item">
                                 <a href=""><span class="fa fa-google-plus"></span></a>
                              </li>
                           </ul>
                        

                        </div>
                     </div>
                  </nav>
               </div>
            </div>
         </div>
      </header>