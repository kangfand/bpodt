<section id="futured">
   <div class="futured container">
      <div class="row nmb">
         <div class="col-sm-8">
            <section id="sec-slider" class="sec-slider">
               <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                     <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                     <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                     <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner">
                     <?php
                     if($agenda<1){
                     echo "<tr><td>Data tidak ditemukan</td></tr>";
                     }
                     else {
                     ?>
                     <div class="carousel-item active">
                        <img class="d-block w-100" src="<?= base_url(); ?>img/news-1.jpg" alt="" height="360px">
                        <div class="carousel-caption d-none d-md-block text-left">
                           
                           <h4>asdasd</h4>
                        </div>
                     </div>
                     
                     <?php
                     foreach ($slider as $data): ?>
                     
                     <div class="carousel-item">
                        <img class="d-block w-100" src="<?= base_url(); ?>upload/<?= $data->images; ?>" alt="First slide" height="360px">
                        <div class="carousel-caption d-none d-md-block text-left">
                           
                           <h4><?php echo $data->title; ?></h4>
                        </div>
                     </div>
                     <?php
                     
                     endforeach;
                     }
                     ?>
                     
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                     <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                     <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                     <span class="carousel-control-next-icon" aria-hidden="true"></span>
                     <span class="sr-only">Next</span>
                  </a>
               </div>
            </section>
         </div>
         <div class="col-sm-4">
            <section id="sec-list" class="sec-list">
               <div class="card" >
                  <ul class="list-group list-group-flush">
                     <li class="list-group-item active" style="padding:18px; background-color:#253C80 ; border-bottom:8px solid #4FA0F1; font-size:18px;">Siaran <strong>Pers</strong></li>
                     
                     <?php
                     if($results<1){
                     echo "<tr><td>Data tidak ditemukan</td></tr>";
                     }
                     else {
                     foreach ($results as $data): ?>
                     <a href="<?= base_url(); ?>infopublik/detail/<?php echo $data->id_news; ?>"><li class="list-group-item list-group-item-action"><?php echo $data->title; ?></li></a>
                     <?php endforeach;
                     } ?>
                     
                  </ul>
               </div>
               <!--
               <div class="card text-center list">
                  <div class="card-block">
                     <h4 class="card-title h5">Individual</h4>
                     <h5 class="card-title h6">100 &euro;/month</h5>
                     <p class="card-text">The flexible place to grow your projects in a community of start-ups, freelancers and entrepreneurs, flex desk.</p>
                     <a href="#" class="btn btn-primary">Find out more</a>
                  </div>
                  
               </div>
               -->
            </section>
         </div>
      </div>
   </div>
</section>