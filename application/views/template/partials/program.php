
<section id="sec-program" class="sec-program">
   <div class="container">
      <h1 class="h4 mb-5 text-center ">Program Prioritas<br>
      <small style="font-size: 14px;">Sesuai Rencana Strategis Kementerian Komunikasi dan Informatika <br>Tahun 2015 - 2019</small>
      </h1>
      <div class="row justify-content-center">
         <div class="col-md-4">
            <div class="card text-center">
               <div class="card-body">
                  <img src="https://www.kominfo.go.id/images/4.png">
                  <!--<h4 class="card-title h5">Individual</h4>-->
                  <h5 class="card-title h6" style="margin-top: 20px;">Judul Ketiga</h5>
                  <p class="card-text">The flexible place to grow your projects in a community of start-ups, freelancers and entrepreneurs, flex desk.</p>
                  <a href="#" class="btn btn-primary">Selengkapnya</a>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="card text-center">
               <div class="card-body">
                  <img src="https://www.kominfo.go.id/images/1.png">
                  <!--<h4 class="card-title h5">Individual</h4>-->
                  <h5 class="card-title h6" style="margin-top: 20px;">Judul Kedua</h5>
                  <p class="card-text">Your own personal area in an open space: desk, drawers, locker and bookcase to accessorise however you like.</p>
                  <a href="#" class="btn btn-primary">Selengkapnya</a>
               </div>
            </div>
         </div>
         <div class="col-md-4">
            <div class="card text-center">
               <div class="card-body justify-content-center">
                  <img src="https://www.kominfo.go.id/images/3.png">
                  <!--<h4 class="card-title h5">Individual</h4>-->
                  <h5 class="card-title h6" style="margin-top: 20px;">Judul Ketiga</h5>
                  <p class="card-text">Your own private space, perfect for work teams of up to 50 people, inside a structure that’s always in motion</p>
                  <a href="#" class="btn btn-primary">Selengkapnya</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>