
<section id="mid-nav">
   <div class="container">
      <div class="row">
         <div class="col-sm-8">
            <ul class="nav nav-tabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active tab-active" href="#profile" role="tab" data-toggle="tab">
                     <h5 class="title">Terbaru</h5>
                     <small>Rangkuman Berita</small>
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">
                     <h5 class="title">Pengumuman</h5>
                     <small>Pengumuman terbaru</small>
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" href="#references" role="tab" data-toggle="tab">
                     <h5 class="title">Unduhan</h5>
                     <small>Laporan terbaru</small>
                  </a>
               </li>
            </ul>
         </div>
         <div class="col-sm-4" style="margin-top: 10px;">
            <form class="form-inline my-2 my-lg-0" style="margin-top: -10px;">
               <input class="form-control mr-sm-2" size="25px" type="text" placeholder="Temukan Sesuatu ..." aria-label="Search">
               <button class="btn btn-primary my-2 my-sm-0" style="width: 100px;" type="submit">Search</button>
            </form>
         </div>
      </div>
   </div>
</section>
<!-- start main -->
<main>
   <div class="container">
      <div class="row">
         <div class="col-md-8">
            
            
          <div class="tab-content">
                     <div role="tabpanel" class="tab-pane fade show active " id="profile">
                        <!-- start post -->
            <div class="row m0">

               <?php
                                    if($news<1){
                                    echo "<tr><td>Data tidak ditemukan</td></tr>";
                                    }
                                    else {
                                    foreach ($news as $data): ?>

            <div class="col-md-6 col-xs-12 box p0" >
                  <div class="row m0 rm">
                     <div class="col-md-12 p0">
                        <div class="news-content">
                           <h5><a href="" style="text-decoration:none;color:#0b56a4;"><?php echo $data->title; ?></a></h5>
                           <?= substr($data->description, 0, 85); ?></p>
                           <span class="date"><?= substr($data->time_updated, 0, 11); ?></span>
                        </div>
                     </div>
                     <div class="col-md-6 p30" >
                        <a href="#" target="_blank"><img src="http://www.bkpm.go.id/assets/icon/attach.png"></a> &nbsp;&nbsp;
                     </div>
                     <div class="col-md-6">
                        <a href="<?= base_url(); ?>infopublik/detail/<?php echo $data->id_news; ?>">
                           <img class="hidden-xs" src="http://www.bkpm.go.id/assets/icon/readmore-slider.png">
                           <!--<img width="30px" class="visible-xs pull-right more-read" src="http://www.bkpm.go.id/assets/icon/read-more-mobile.png">-->
                        </a>
                     </div>
                  </div>
               </div> 
                                    
                                    
                                    <?php
                                    
                                    endforeach;
                                    } ?>
                                    


             
            
            </div>
            <!-- end post-->
                     </div>
              <!-- Pengumuman -->
                              <div role="tabpanel" class="tab-pane fade" id="buzz">
                                 <div class="row">

                                
                                    <?php
                                    if($agenda<1){
                                    echo "<tr><td>Data tidak ditemukan</td></tr>";
                                    }
                                    else {
                                    foreach ($agenda as $data): ?>
                                      <div class="col-sm-6 mb-3">
                                    <div class="card">
                                       <img class="card-img-top" src="<?= base_url(); ?>upload/<?= $data->images; ?>" alt="Card image cap">
                                       <div class="card-body">
                                          <a href="<?= base_url(); ?>infopublik/detail/<?php echo $data->id_news; ?>"><h4 class="card-title"><?php echo $data->title; ?></h4></a>
                                       </div>
                                       <div class="card-footer">
                                          <small class="text-muted"><?php echo $data->time_updated; ?></small>
                                       </div>
                                    </div>
                                 </div>
                                    
                                    <?php
                                    
                                    endforeach;
                                    } ?>
                                    
                                    </div>
                              
                           
                              </div>
                              <!-- Unduhan -->
                              <div role="tabpanel" class="tab-pane fade" id="references">
                                 kosong
                              </div>
                     </div> <!-- end tab content -->

         </div>
         <div class="col-sm-4">
            <div class="sidebar">
                     <a data-toggle="modal" data-target=".bd-example-modal-lg"> <img src="img/video.png" style="margin-bottom: 40px;" class="img-fluid"></a>
              
                     <!-- Berita Public Kominfo -->
                     <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
                     <div id="gpr-kominfo-widget-container"></div>
                     
                    
                     <!--
                     <div class="card">
                        <div class="card-header">
                           Profile
                        </div>
                        <ul class="list-group list-group-flush">
                           <li class="list-group-item"><a href="">Sejarah</a></li>
                           <li class="list-group-item"><a href="">Tugas dan Fugsi</a></li>
                           <li class="list-group-item"><a href="">Struktur Organisasi</a></li>
                           <li class="list-group-item"><a href="">Unit Kerja</a></li>
                        </ul>
                     </div> -->
                  </div>
         </div>
      </div>
   </div>
         
         
      </main>
   
      