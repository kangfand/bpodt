<?php

class Msubscribers extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function record_count() {

            return $this->db->count_all("subscribers");
    }

     public function fetch_subscribers($limit, $start) {
        $this->db->limit($limit, $start);

        $this->db->order_by('id_subscribers', 'DESC'); 
        $query = $this->db->get("subscribers");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }



        public function add()
        {
                $email = $this->input->post('email');
                $data = array(
                                'email' => $email                                        
                            );
                $this->db->insert('subscribers', $data);
                header('Location: ' . $_SERVER['HTTP_REFERER']);
        }

        public function add_images()
        {
                $images = $_FILES['images']['name'];
                $id_subscribers = $this->input->post('id_subscribers');
                $data = array(
                                'images' => $images,
                                'id_subscribers' => $id_subscribers             
                              );
                $this->db->insert('subscribers_detail', $data);
        }



        function edit($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM subscribers WHERE id_subscribers= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function detail_front($id){
                $instansi = $this->uri->slash_segment(3);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM subscribers WHERE id_subscribers= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function detail($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM subscribers WHERE id_subscribers= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function detail_images($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM subscribers_detail WHERE id_subscribers= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function edit_images_priority($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM subscribers_detail WHERE id_subscribers= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function update_images_priority($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM subscribers_detail WHERE id_subscribers= '$keyword'");
               // echo $keyword;               

                $this->db->where('id_subscribers', $keyword);
                $this->db->update('subscribers', $data);
        }

        function update(){


                $images_cover = $this->input->post('images_cover');
                $title = $this->input->post('title');
                $description = $this->input->post('description');
                $id_category = $this->input->post('id_category');


                $keyword = $this->input->post('id_subscribers');
                $data = array(

                                        'images' => $images_cover,
                                        'title' => $title,
                                        'description' => $description,
                                        'id_category' => $id_category

                                        
                                         );


                $this->db->where('id_subscribers', $keyword);
                $this->db->update('subscribers', $data);
                redirect('admin/subscribers');
                //echo "Hello";

        }

        function delete($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `subscribers` WHERE `subscribers`.`id_subscribers` = '$keyword'");

                redirect('admin/subscribers');
        }

        function delete_images($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `subscribers_detail` WHERE `subscribers_detail`.`id_subscribers_detail` = '$keyword'");
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                //redirect('admin/subscribers');
        }


        function get_five(){

                $query = $this->db->query("SELECT * FROM subscribers ORDER BY id_subscribers DESC limit 5 ");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }
}