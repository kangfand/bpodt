<?php

class Mportfolio extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function record_count() {

            return $this->db->count_all("portfolio");
    }

     public function fetch_portfolio($limit, $start) {
        $this->db->limit($limit, $start);

        $this->db->order_by('priority', 'ASC'); 
        $this->db->where('portfolio.id_building = building.id_building');
        $query = $this->db->get("portfolio, building");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }



        public function add()
        {

                $name = $this->input->post('name');
                $images_cover = $_FILES['images_cover']['name'];
                $client = $this->input->post('client');
                $year = $this->input->post('year');
                $id_building = $this->input->post('id_building');
                $location = $this->input->post('location');
                $area = $this->input->post('area');
                $architect = $this->input->post('architect');
                $constructor = $this->input->post('constructor');
                $estimator = $this->input->post('estimator');
                $drafter = $this->input->post('drafter');
                $visualizer = $this->input->post('visualizer');
                $priority = $this->input->post('priority');

                $keyword = $this->input->post('unique');
                $data = array(
                                        'name' => $name,
                                        'images_cover' => $images_cover,
                                        'client' => $client,
                                        'year' => $year,
                                        'id_building' => $id_building,
                                        'location' => $location,
                                        'area' => $area,
                                        'architect' => $architect,
                                        'constructor' => $constructor,
                                        'estimator' => $estimator,
                                        'drafter' => $drafter,
                                        'visualizer' => $visualizer,
                                        'priority' => $priority
                                        
                                         );
                $this->db->insert('portfolio', $data);
        }

        public function add_images()
        {
                $images = $_FILES['images']['name'];
                $id_portfolio = $this->input->post('id_portfolio');
                $data = array(
                                'images' => $images,
                                'id_portfolio' => $id_portfolio             
                              );
                $this->db->insert('portfolio_detail', $data);
        }



        function edit($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio WHERE id_portfolio= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function detail($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio WHERE id_portfolio= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function detail_projects(){
                $instansi = $this->uri->slash_segment(3);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio, building WHERE 
                portfolio.id_building=building.id_building AND id_portfolio= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function detail_images($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio_detail WHERE id_portfolio= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function projects_detail(){
                $instansi = $this->uri->slash_segment(3);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio_detail WHERE id_portfolio= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function edit_images_priority($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio_detail WHERE id_portfolio= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function update_images_priority($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM portfolio_detail WHERE id_portfolio= '$keyword'");
               // echo $keyword;               

                $this->db->where('id_portfolio', $keyword);
                $this->db->update('portfolio', $data);
        }

        function update(){

                $name = $this->input->post('name');
                $images_cover = $_FILES['images_cover']['name'];
                $client = $this->input->post('client');
                $year = $this->input->post('year');
                $id_building = $this->input->post('id_building');
                $location = $this->input->post('location');
                $area = $this->input->post('area');
                $architect = $this->input->post('architect');
                $constructor = $this->input->post('constructor');
                $estimator = $this->input->post('estimator');
                $drafter = $this->input->post('drafter');
                $visualizer = $this->input->post('visualizer');
                $priority = $this->input->post('priority');

                $keyword = $this->input->post('id_portfolio');
                $data = array(
                                        'name' => $name,
                                        'images_cover' => $images_cover,
                                        'client' => $client,
                                        'year' => $year,
                                        'id_building' => $id_building,
                                        'location' => $location,
                                        'area' => $area,
                                        'architect' => $architect,
                                        'constructor' => $constructor,
                                        'estimator' => $estimator,
                                        'drafter' => $drafter,
                                        'visualizer' => $visualizer,
                                        'priority' => $priority
                                        
                                         );


                $this->db->where('id_portfolio', $keyword);
                $this->db->update('portfolio', $data);
                redirect('admin/portfolio');
                //echo "Hello";

        }

        function delete($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `portfolio` WHERE `portfolio`.`id_portfolio` = '$keyword'");

                redirect('admin/portfolio');
        }

        function get_all($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("SELECT * FROM portfolio");

                redirect('admin/portfolio');
        }

        function delete_images($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `portfolio_detail` WHERE `portfolio_detail`.`id_portfolio_detail` = '$keyword'");
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                //redirect('admin/portfolio');
        }



        function get_slider(){

                $query = $this->db->query("SELECT * FROM portfolio, building
                 WHERE portfolio.id_building=building.id_building  ORDER BY priority ASC limit 0,5");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function get_other_portfolio(){

                $query = $this->db->query("SELECT * FROM portfolio ORDER BY priority ASC limit 0,9");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


}