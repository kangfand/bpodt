<?php

class Mcontact extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function record_count() {

            return $this->db->count_all("contact");
    }

     public function fetch_contact($limit, $start) {
        $this->db->limit($limit, $start);

        $this->db->order_by('id_contact', 'DESC'); 
        $query = $this->db->get("contact");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

        public function add()
        {
                $name = $this->input->post('name');
                $email = $this->input->post('email');
                $company = $this->input->post('company');
                $message = $this->input->post('message');
                $data = array(
                                'name' => $name,
                                'email' => $email,  
                                'company' => $company,  
                                'message' => $message                                        
                            );
                $this->db->insert('contact', $data);
                header('Location: ' . $_SERVER['HTTP_REFERER']);
        }


        function delete($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `contact` WHERE `contact`.`id_contact` = '$keyword'");

                redirect('admin/contact');
        }


}