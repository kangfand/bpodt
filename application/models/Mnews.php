<?php

class Mnews extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function record_count() {

            return $this->db->count_all("news");
    }

     public function fetch_news($limit, $start) {
        $this->db->limit($limit, $start);

        $this->db->order_by('id_news', 'DESC'); 
        $query = $this->db->get("news");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }



        public function add()
        {

                $images_cover = $_FILES['images_cover']['name'];
                $title = $this->input->post('title');
                $description = $this->input->post('article');
                $id_category = $this->input->post('id_category');
                $data = array(
                                        'images' => $images_cover,
                                        'title' => $title,
                                        'description' => $description,
                                        'id_category' => $id_category
                                        
                                         );
                $this->db->insert('news', $data);
        }

        public function add_images()
        {
                $images = $_FILES['images']['name'];
                $id_news = $this->input->post('id_news');
                $data = array(
                                'images' => $images,
                                'id_news' => $id_news             
                              );
                $this->db->insert('news_detail', $data);
        }



        function edit($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM news WHERE id_news= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function detail_front($id){
                $instansi = $this->uri->slash_segment(3);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM news WHERE id_news= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function detail($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM news WHERE id_news= '$id'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function detail_images($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM news_detail WHERE id_news= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function edit_images_priority($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM news_detail WHERE id_news= '$keyword'");
               // echo $keyword;               

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function update_images_priority($id){
                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $query = $this->db->query("SELECT * FROM news_detail WHERE id_news= '$keyword'");
               // echo $keyword;               

                $this->db->where('id_news', $keyword);
                $this->db->update('news', $data);
        }

        function update(){


                $images_cover = $this->input->post('images_cover');
                $title = $this->input->post('title');
                $description = $this->input->post('article');
                $id_category = $this->input->post('id_category');


                $keyword = $this->input->post('id_news');
                $data = array(

                                        'images' => $images_cover,
                                        'title' => $title,
                                        'description' => $description,
                                        'id_category' => $id_category

                                        
                                         );


                $this->db->where('id_news', $keyword);
                $this->db->update('news', $data);
                redirect('admin/news');
                //echo "Hello";

        }

        function delete($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `news` WHERE `news`.`id_news` = '$keyword'");

                redirect('admin/news');
        }

        function delete_images($id){

                $instansi = $this->uri->slash_segment(4);
                $key1 = strrev($instansi);
                $key2 = substr($key1, 1, 150);
                $key3 = strrev($key2);                
                $keyword = urldecode($key3);
                $this->db->query("DELETE FROM `news_detail` WHERE `news_detail`.`id_news_detail` = '$keyword'");
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                //redirect('admin/news');
        }


        function get_five(){

                $query = $this->db->query("SELECT * FROM news ORDER BY id_news DESC limit 5 ");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function get_pers(){

                $query = $this->db->query("SELECT * FROM `news` WHERE id_category = 5 ORDER BY id_news DESC limit 6");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function get_berita(){

                $query = $this->db->query("SELECT * FROM `news` WHERE id_category = 2 ORDER BY id_news DESC limit 10");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }

        function get_agenda(){

                $query = $this->db->query("SELECT * FROM `news` WHERE id_category = 6 ORDER BY id_news DESC limit 5");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }


        function get_slider(){

                $query = $this->db->query("SELECT * FROM `news` WHERE id_category = 7 ORDER BY id_news DESC limit 3");

                if($query->num_rows()>0)
                {

                                foreach ($query->result() as $data)
                                {
                                        $hasil[] = $data;
                                }
                                
                return $hasil;

                }
        }
}