<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class infopublik extends CI_Controller {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("Mportfolio");
        $this->load->library("pagination");
    }
	public function index()
	{
        $data["results"] = $this->Mnews->get_pers();
		$data["agenda"] = $this->Mnews->get_agenda();
		$data["slider"] = $this->Mnews->get_slider();

		$this->load->view('template/header');
		$this->load->view('template/pages',$data);
		$this->load->view('template/footer');
	}

	public function siaranpers()
	{
        $data["news"] = $this->Mnews->get_pers();
		

		$this->load->view('template/header');
		$this->load->view('template/siaranpers',$data);
		$this->load->view('template/footer');
	}

	public function laporan()
	{
        $data["results"] = $this->Mnews->get_pers();
		$data["agenda"] = $this->Mnews->get_agenda();
		$data["slider"] = $this->Mnews->get_slider();

		$this->load->view('template/header');
		$this->load->view('template/pages',$data);
		$this->load->view('template/footer');
	}

	public function agenda()
	{
        $data["results"] = $this->Mnews->get_pers();
		$data["agenda"] = $this->Mnews->get_agenda();
		$data["slider"] = $this->Mnews->get_slider();

		$this->load->view('template/header');
		$this->load->view('template/pages',$data);
		$this->load->view('template/footer');
	}

	public function detail($id){

		$data['news'] = $this->Mnews->detail($id);
		

		$this->load->view('template/header');
		$this->load->view('template/detail',$data);
		$this->load->view('template/footer');
	}
}
