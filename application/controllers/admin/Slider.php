<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Home';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

		$this->load->view('admin/template/content-sample');

		$this->load->view('admin/template/footer');
	}
}
