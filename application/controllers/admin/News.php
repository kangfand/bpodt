<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("Mportfolio");
        $this->load->library("pagination");
    }


	public function index()
	{
		$data['title'] = 'news';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

		$config = array();
        $config["base_url"] = base_url() . "admin/news/index";
        $config["total_rows"] = $this->Mnews->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Mnews->
            fetch_news($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

		$this->load->view('admin/news/news', $data);

		$this->load->view('admin/template/footer');
	}


	public function edit($id=0)
	{
		
		$data['title'] = 'Edit news';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['category'] = $this->Mcategory->get_all();
		$data['results'] = $this->Mnews->edit($id);
		$this->load->view('admin/news/edit', $data);

			if($this->input->post('submit')){
			$this->load->model('Mnews');
			$this->Mnews->update();
			redirect('news');
		}
		$this->load->view('admin/template/footer');

	}

	public function detail($id=0)
	{
		
		$data['title'] = 'Detail news';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['results'] = $this->Mnews->detail($id);
		$data['detail_images'] = $this->Mnews->detail_images($id);
		$this->load->view('admin/news/detail', $data);

		$this->load->view('admin/template/footer');

	}
	public function edit_images_priority($id=0)
	{
		
		$data['title'] = 'Edit Images Priority';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['results'] = $this->Mnews->detail($id);
		$data['detail_images'] = $this->Mnews->detail_images($id);
		$this->load->view('admin/news/images_priority', $data);

			if($this->input->post('submit')){
			$this->Mnews->update_images_priority();
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}

		$this->load->view('admin/template/footer');

	}

	public function add_images()
	{
		
		$data['title'] = 'Add Images news';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

			if($this->input->post('submit')){
				// manual upload file
     		$file_tmp =$_FILES['images']['tmp_name'];     		
      		$file_name = $_FILES['images']['name'];

      		move_uploaded_file($file_tmp,"upload/".$file_name);

			$this->load->model('Mnews');
			$this->Mnews->add_images();
			$id_news = $this->input->post('id_news');
			redirect('admin/news/detail/'. $id_news . '');
		}

		$data['building'] = $this->Mbuilding->get_all();
		$this->load->view('admin/news/add_images', $data);

		$this->load->view('admin/template/footer');

	}

	public function add()
	{
		
		$data['title'] = 'Add news';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

			if($this->input->post('submit')){
				// manual upload file
     		$file_tmp =$_FILES['images_cover']['tmp_name'];     		
      		$file_name = $_FILES['images_cover']['name'];

      		move_uploaded_file($file_tmp,"upload/".$file_name);

			$this->load->model('Mnews');
			$this->Mnews->add();
			redirect('admin/news');
		}

		$data['category'] = $this->Mcategory->get_all();
		$this->load->view('admin/news/add', $data);

		$this->load->view('admin/template/footer');

	}


	// function update(){
	// 	if($this->input->post('submit')){
	// 		$this->load->model('Mnews');
	// 		$this->Mnews->update();
	// 		redirect('news');
	// 	}
	// 	else {
	// 		echo "Kosong";
	// 	}
	// }

	function delete($id){	

		$this->load->model('Mnews');
		$data['hasil'] = $this->Mnews->delete($id);
		redirect('admin/news');
	}


	function delete_images($id){	
		$this->load->model('Mnews');
		$data['hasil'] = $this->Mnews->delete_images($id);
		//redirect('admin/news');
	}

	function export(){
		$this->load->model('Mnews');
		$data['hasil'] = $this->Mnews->get_all();
		$this->load->view('export/news', $data);

	}
}
