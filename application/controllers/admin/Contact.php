<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("Mportfolio");
        $this->load->library("pagination");
    }


	public function index()
	{
		$data['title'] = 'contact';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

		$config = array();
        $config["base_url"] = base_url() . "admin/contact/index";
        $config["total_rows"] = $this->Mcontact->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Mcontact->
            fetch_contact($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

		$this->load->view('admin/contact/contact', $data);

		$this->load->view('admin/template/footer');
	}



	function delete($id){	

		$this->load->model('Mcontact');
		$data['hasil'] = $this->Mcontact->delete($id);
		redirect('admin/contact');
	}



}
