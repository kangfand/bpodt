<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribers extends CI_Controller {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("Mportfolio");
        $this->load->library("pagination");
    }


	public function index()
	{
		$data['title'] = 'subscribers';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

		$config = array();
        $config["base_url"] = base_url() . "admin/subscribers/index";
        $config["total_rows"] = $this->Msubscribers->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Msubscribers->
            fetch_subscribers($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

		$this->load->view('admin/subscribers/subscribers', $data);

		$this->load->view('admin/template/footer');
	}


	public function edit($id=0)
	{
		
		$data['title'] = 'Edit subscribers';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['category'] = $this->Mcategory->get_all();
		$data['results'] = $this->Msubscribers->edit($id);
		$this->load->view('admin/subscribers/edit', $data);

			if($this->input->post('submit')){
			$this->load->model('Msubscribers');
			$this->Msubscribers->update();
			redirect('subscribers');
		}
		$this->load->view('admin/template/footer');

	}

	public function detail($id=0)
	{
		
		$data['title'] = 'Detail subscribers';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['results'] = $this->Msubscribers->detail($id);
		$data['detail_images'] = $this->Msubscribers->detail_images($id);
		$this->load->view('admin/subscribers/detail', $data);

		$this->load->view('admin/template/footer');

	}
	public function edit_images_priority($id=0)
	{
		
		$data['title'] = 'Edit Images Priority';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['results'] = $this->Msubscribers->detail($id);
		$data['detail_images'] = $this->Msubscribers->detail_images($id);
		$this->load->view('admin/subscribers/images_priority', $data);

			if($this->input->post('submit')){
			$this->Msubscribers->update_images_priority();
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}

		$this->load->view('admin/template/footer');

	}

	public function add_images()
	{
		
		$data['title'] = 'Add Images subscribers';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

			if($this->input->post('submit')){
				// manual upload file
     		$file_tmp =$_FILES['images']['tmp_name'];     		
      		$file_name = $_FILES['images']['name'];

      		move_uploaded_file($file_tmp,"upload/".$file_name);

			$this->load->model('Msubscribers');
			$this->Msubscribers->add_images();
			$id_subscribers = $this->input->post('id_subscribers');
			redirect('admin/subscribers/detail/'. $id_subscribers . '');
		}

		$data['building'] = $this->Mbuilding->get_all();
		$this->load->view('admin/subscribers/add_images', $data);

		$this->load->view('admin/template/footer');

	}

	public function add()
	{
			$this->load->model('Msubscribers');
			$this->Msubscribers->add();
			
			header('Location: ' . $_SERVER['HTTP_REFERER']);
	}


	// function update(){
	// 	if($this->input->post('submit')){
	// 		$this->load->model('Msubscribers');
	// 		$this->Msubscribers->update();
	// 		redirect('subscribers');
	// 	}
	// 	else {
	// 		echo "Kosong";
	// 	}
	// }

	function delete($id){	

		$this->load->model('Msubscribers');
		$data['hasil'] = $this->Msubscribers->delete($id);
		redirect('admin/subscribers');
	}


	function delete_images($id){	
		$this->load->model('Msubscribers');
		$data['hasil'] = $this->Msubscribers->delete_images($id);
		//redirect('admin/subscribers');
	}

	function export(){
		$this->load->model('Msubscribers');
		$data['hasil'] = $this->Msubscribers->get_all();
		$this->load->view('export/subscribers', $data);

	}
}
