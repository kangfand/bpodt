<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {    

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("Mportfolio");
        $this->load->library("pagination");
    }

	public function index()
	{
		$data['title'] = 'Portfolio';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

		$config = array();
        $config["base_url"] = base_url() . "admin/portfolio/index";
        $config["total_rows"] = $this->Mportfolio->record_count();
        $config["per_page"] = 10;
        $config["uri_segment"] = 4;

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->Mportfolio->
            fetch_portfolio($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

		$this->load->view('admin/portfolio/portfolio', $data);

		$this->load->view('admin/template/footer');
	}


	public function edit($id=0)
	{
		
		$data['title'] = 'Edit Portfolio';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['building'] = $this->Mbuilding->get_all();
		$data['results'] = $this->Mportfolio->edit($id);
		$this->load->view('admin/portfolio/edit', $data);

			if($this->input->post('submit')){
			$this->load->model('Mportfolio');
			$this->Mportfolio->update();
			redirect('portfolio');
		}
		$this->load->view('admin/template/footer');

	}

	public function detail($id=0)
	{
		
		$data['title'] = 'Detail Portfolio';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['results'] = $this->Mportfolio->detail($id);
		$data['detail_images'] = $this->Mportfolio->detail_images($id);
		$this->load->view('admin/portfolio/detail', $data);

		$this->load->view('admin/template/footer');

	}
	public function edit_images_priority($id=0)
	{
		
		$data['title'] = 'Edit Images Priority';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/template/navigation');

		$data['results'] = $this->Mportfolio->detail($id);
		$data['detail_images'] = $this->Mportfolio->detail_images($id);
		$this->load->view('admin/portfolio/images_priority', $data);

			if($this->input->post('submit')){
			$this->Mportfolio->update_images_priority();
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}

		$this->load->view('admin/template/footer');

	}

	public function add_images()
	{
		
		$data['title'] = 'Add Images Portfolio';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

			if($this->input->post('submit')){
				// manual upload file
     		$file_tmp =$_FILES['images']['tmp_name'];     		
      		$file_name = $_FILES['images']['name'];

      		move_uploaded_file($file_tmp,"upload/".$file_name);

			$this->load->model('Mportfolio');
			$this->Mportfolio->add_images();
			$id_portfolio = $this->input->post('id_portfolio');
			redirect('admin/portfolio/detail/'. $id_portfolio . '');
		}

		$data['building'] = $this->Mbuilding->get_all();
		$this->load->view('admin/portfolio/add_images', $data);

		$this->load->view('admin/template/footer');

	}

	public function add()
	{
		
		$data['title'] = 'Add Portfolio';
		$this->load->view('admin/template/header',$data);

		$this->load->view('admin/template/navigation');

			if($this->input->post('submit')){
				// manual upload file
     		$file_tmp =$_FILES['images_cover']['tmp_name'];     		
      		$file_name = $_FILES['images_cover']['name'];

      		move_uploaded_file($file_tmp,"upload/".$file_name);

			$this->load->model('Mportfolio');
			$this->Mportfolio->add();
			redirect('admin/portfolio');
		}

		$data['building'] = $this->Mbuilding->get_all();
		$this->load->view('admin/portfolio/add', $data);

		$this->load->view('admin/template/footer');

	}


	// function update(){
	// 	if($this->input->post('submit')){
	// 		$this->load->model('Mportfolio');
	// 		$this->Mportfolio->update();
	// 		redirect('portfolio');
	// 	}
	// 	else {
	// 		echo "Kosong";
	// 	}
	// }

	function delete($id){	

		$this->load->model('Mportfolio');
		$data['hasil'] = $this->Mportfolio->delete($id);
		redirect('admin/portfolio');
	}


	function delete_images($id){	
		$this->load->model('Mportfolio');
		$data['hasil'] = $this->Mportfolio->delete_images($id);
		//redirect('admin/portfolio');
	}

	function export(){
		$this->load->model('Mportfolio');
		$data['hasil'] = $this->Mportfolio->get_all();
		$this->load->view('export/portfolio', $data);

	}

}