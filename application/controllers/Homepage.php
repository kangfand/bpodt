<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->model("Mportfolio");
        $this->load->library("pagination");
    }
	public function index()
	{
        $data["results"] = $this->Mnews->get_pers();
		$data["agenda"] = $this->Mnews->get_agenda();
		$data["slider"] = $this->Mnews->get_slider();
		$data["news"] = $this->Mnews->get_berita();

		$this->load->view('template/header');
		$this->load->view('template/partials/futured',$data);
		$this->load->view('template/home',$data);
		$this->load->view('template/partials/tautan');
		$this->load->view('template/footer');
	}
}
