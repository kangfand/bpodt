<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontak extends CI_Controller {

	public function __construct() {
        parent:: __construct();
    }
	public function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/contact');
		$this->load->view('template/partials/tautan');
		$this->load->view('template/footer');
	}
}
