<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['title'] = 'Login Administrator';
		$this->load->view('admin/template/header_login',$data);
		$this->load->view('admin/user/login');
		$this->load->view('admin/template/footer');

	}

	public function check_authority(){
	$this->load->helper('url');
		$email = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);
		$this->db->where('username', $email);
		$this->db->where('password', md5($password));
		$query = $this->db->get('admin');	

		if($query->num_rows()==1)
		{
			foreach ($query->result() as $row)
				{	
				    $username =  $row->username;
				    $password = $row->password;
					$newdata = array(
						     	'username' => $username,
						        'password' => $password,
						        'logged_in' => TRUE
						);
						$this->session->set_userdata($newdata);
						$this->load->helper('url');

			       		redirect('admin/home', true); //redirect ke halaman profile
				}
		}
		else{		
		$data['title'] = 'Login Administrator';
		$this->load->view('admin/template/header',$data);
		$this->load->view('admin/user/login');
		$this->load->view('admin/template/footer');
		}
	}
}